package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day14Tests extends munit.FunSuite {
  private val test = List(
 //  0123456789
    "O....#....",
    "O.OO#....#",
    ".....##...",
    "OO.#O....O",
    ".O.....O#.",
    "O.#..O.#.#",
    "..O..#O..O",
    ".......O..",
    "#....###..",
    "#OO..#...."
  )

  test ("parse") {
    val data = List(
      //  0123456789
      "O....#....",
      "O.OO#....#",
      ".....##...",
      "OO.#O....O"
    )
    val expected = List(
      Stone.Round(0,0), Stone.Stop(5,0 ),
      Stone.Round(0,1), Stone.Round(2,1), Stone.Round(3,1), Stone.Stop(4,1), Stone.Stop(9,1),
      Stone.Stop(5,2), Stone.Stop(6,2),
      Stone.Round(0,3), Stone.Round(1,3), Stone.Stop(3,3), Stone.Round(4,3), Stone.Round(9,3)
    )
    assertEquals( parseData(data), expected )
  }

  test( "split into columns" ) {
    val data = List(
   //  0123456789
      "O....",
      "O.OO#",
      ".....",
      "OO.#O"
    )
    val expected = List(
      List(Stone.Round(0,0), Stone.Round(0,1), Stone.Round(0,3)),
      List(Stone.Round(1,3)),
      List(Stone.Round(2,1)),
      List(Stone.Round(3,1), Stone.Stop(3,3)),
      List(Stone.Stop(4,1), Stone.Round(4,3))
    )
    assertEquals( splitIntoColumns(parseData(data)), expected )

  }

  test("move north column") {
    val in = List(Stone.Stop(4, 1), Stone.Round(4, 3))
    val out = List(Stone.Stop(4, 1), Stone.Round(4, 2))
    assertEquals( moveNorth(in), out )
  }

  test("move north table") {
    val moved = List(
      "OOOO.#.O..",
      "OO..#....#",
      "OO..O##..O",
      "O..#.OO...",
      "........#.",
      "..#....#.#",
      "..O..#.O.O",
      "..O.......",
      "#....###..",
      "#....#...."
    )
    val in = splitIntoColumns(parseData(test))
    val out = splitIntoColumns(parseData(moved))
    assertEquals( in.map( moveNorth ), out )
  }

  test("move north1 table") {
    val moved = List(
      "OOOO.#.O..",
      "OO..#....#",
      "OO..O##..O",
      "O..#.OO...",
      "........#.",
      "..#....#.#",
      "..O..#.O.O",
      "..O.......",
      "#....###..",
      "#....#...."
    )
    val in = parseData(test)
    val out = parseData(moved)
    assertEquals(moveNorth1(in,10,10).toSet, out.toSet)
  }

  test("move south table") {
    val moved = List(
      ".....#....",
      "....#....#",
      "...O.##...",
      "...#......",
      "O.O....O#O",
      "O.#..O.#.#",
      "O....#....",
      "OO....OO..",
      "#OO..###..",
      "#OO.O#...O"
    )

    val in = parseData(test)
    val out = parseData(moved)
    assertEquals(moveSouth(in, 10, 10).toSet, out.toSet)
  }

  test("move west table") {
    val moved = List(
      "O....#....",
      "OOO.#....#",
      ".....##...",
      "OO.#OO....",
      "OO......#.",
      "O.#O...#.#",
      "O....#OO..",
      "O.........",
      "#....###..",
      "#OO..#...."
    )
    val in = parseData(test)
    val out = parseData(moved)
    assertEquals(moveWest(in, 10, 10).toSet, out.toSet)
  }

  test("move east table") {
    val moved = List(
      "....O#....",
      ".OOO#....#",
      ".....##...",
      ".OO#....OO",
      "......OO#.",
      ".O#...O#.#",
      "....O#..OO",
      ".........O",
      "#....###..",
      "#..OO#...."
    )
    val in = parseData(test)
    val out = parseData(moved)
    assertEquals(moveEast(in, 10, 10).toSet, out.toSet)
  }

  test("Day14 - Part1") {
    assertEquals(step1(test), 136)
  }

  test("Day14 - Part2") {
    assertEquals(step2(test), 64)
  }

}