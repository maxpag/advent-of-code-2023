package org.maxpagani

enum Stone( val x: Int, val y: Int):
  case Round(override val x: Int, override val y: Int) extends Stone(x, y)
  case Stop(override val x: Int, override val y: Int) extends Stone(x, y)

def compare( a: Stone, b: Stone ) : Boolean =
  a.y < b.y || (a.y == b.y && a.x < b.x)

def parseData( data: List[String] ) : List[Stone] =
  data.zipWithIndex.flatMap((line, y) => line.zipWithIndex.map((c, x) => c match {
    case 'O' => Some(Stone.Round(x, y))
    case '#' => Some(Stone.Stop(x, y))
    case _ => None
  })).flatten

def splitIntoColumns( data: List[Stone] ) : List[List[Stone]] =
  data.groupBy(_.x).values.toList

def moveNorth1( data: List[Stone], width: Int, height: Int ) : List[Stone] = {
  val levels : Array[Int] = Array.fill(width)(0)
  data.sortWith((a,b)=>a.y<b.y).map {
    case Stone.Round(x, y) =>
      val newY = levels(x)
      levels(x) = newY + 1
      Stone.Round(x, newY)
    case Stone.Stop(x, y) =>
      levels(x) = y + 1
      Stone.Stop(x, y)
  }
}

def moveWest( data: List[Stone], width: Int, height: Int ) : List[Stone] = {
  val levels : Array[Int] = Array.fill(height)(0)
  data.sortWith((a,b)=>a.x < b.x).map {
    case Stone.Round(x, y) =>
      val newX = levels(y)
      levels(y) = newX + 1
      Stone.Round(newX, y)
    case Stone.Stop(x, y) =>
      levels(y) = x + 1
      Stone.Stop(x, y)
  }
}

def moveEast( data: List[Stone], width: Int, height: Int ) : List[Stone] = {
  val levels : Array[Int] = Array.fill(height)(width-1)
  data.sortWith((a,b)=>a.x > b.x).map {
    case Stone.Round(x, y) =>
      val newX = levels(y)
      levels(y) = newX - 1
      Stone.Round(newX, y)
    case Stone.Stop(x, y) =>
      levels(y) = x - 1
      Stone.Stop(x, y)
  }
}

def moveSouth( data: List[Stone], width: Int, height: Int ) : List[Stone] = {
  val levels : Array[Int] = Array.fill(width)(height-1)
  data.sortWith((a,b)=>a.y > b.y).map {
    case Stone.Round(x, y) =>
      val newY = levels(x)
      levels(x) = newY - 1
      Stone.Round(x, newY)
    case Stone.Stop(x, y) =>
      levels(x) = y - 1
      Stone.Stop(x, y)
  }
}


def moveNorth( column: List[Stone] ) : List[Stone] = {
  case class Acc(result: List[Stone], y: Int)
  column.foldLeft(Acc(List.empty, 0))((acc, stone) => stone match {
    case Stone.Round(x, y) => Acc(Stone.Round(x, acc.y) :: acc.result, acc.y + 1)
    case Stone.Stop(x, y) => Acc(Stone.Stop(x, y) :: acc.result, y+1)
  }).result
}

def computeWeight( column: List[Stone], maxHeight: Int ) : Int =
  column.foldLeft( 0 )( (acc, stone) => stone match {
    case Stone.Round( _, y ) => acc + maxHeight - y
    case Stone.Stop( _, _ ) => acc
  } )

def step1( data: List[String] ) : Int =
  val stones = splitIntoColumns( parseData( data ) )
  val maxHeight = data.length
  stones.map( column => computeWeight( moveNorth( column ), maxHeight ) ).sum

def step2( data: List[String] ) : Int = {
  val stones = parseData( data )
  val width = data.head.length
  val height = data.length
  val result = (1 to 1000000000).foldLeft( stones )(
    (s,n) => {
      if(n % 10000 == 0) println(s"${n.toDouble/10000000}%")
      moveNorth1( moveWest( moveSouth( moveEast( s, width, height ), width, height ), width, height ), width, height )
    }
  )
  splitIntoColumns(result ).map( column => computeWeight( column, height ) ).sum
}
