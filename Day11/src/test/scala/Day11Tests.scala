package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2


class Day11Tests extends munit.FunSuite {
  private val test = List(
  // 0123456789
    "...#......", // 0
    ".......#..", // 1
    "#.........", // 2
    "..........", // 3
    "......#...", // 4
    ".#........", // 5
    ".........#", // 6
    "..........", // 7
    ".......#..", // 8
    "#...#....."  // 9
  )

  test("Day11 - distance") {
    val g5 = Galaxy( 6, 1 )
    assertEquals(distance(g5,g5), 0)
    val g9 = Galaxy( 11, 5)
    assertEquals(distance(g5,g9), 9)
    assertEquals(distance(g9,g5), 9)


    val g1 = Galaxy( 0, 4 )
    val g7 = Galaxy( 10, 9 )
    val g3 = Galaxy( 2, 0 )
    val g6 = Galaxy( 7, 12 )
    val g8 = Galaxy( 11, 0)

    assertEquals( distance( g1, g7 ), 15 )
    assertEquals( distance( g3, g6 ), 17 )
    assertEquals( distance( g8, g9 ), 5 )

  }

  test("Day11 - expansion") {
    val emptyRows = List(3,7)
    val emptyCols = List(2,5,8)
    assertEquals( expand( 5, emptyRows, 2), 6 )
    assertEquals( expand( 1, emptyCols, 2), 1 )
    assertEquals( expand( 6, emptyCols, 2), 8 )
    assertEquals( expand( 9, emptyCols, 2), 12 )
  }

  test( "Day11 - empties" ) {
    val emptyRows = List(3,7)
    val emptyCols = List(2,5,8)
    assertEquals( getEmptyRows( test ), emptyRows )
    assertEquals( getEmptyColumns( test ), emptyCols )
  }

  test( "Day11 - find galaxies") {
    val galaxies = findGalaxies( test )
    assertEquals( galaxies.size, 9 )
    val check = List(
      Galaxy(0,3),
      Galaxy( 1, 7 ),
      Galaxy( 2, 0 ),
      Galaxy( 4, 6 ),
      Galaxy( 5, 1 ),
      Galaxy( 6, 9 ),
      Galaxy( 8, 7 ),
      Galaxy( 9, 0 ),
      Galaxy( 9, 4 )
    )
    assertEquals( galaxies, check )
  }

  test("Day11 - Part1") {
    assertEquals(step1(test), 374)
  }

  test("Day11 - Part2") {
    assertEquals(step1(test,10), 1030)
    assertEquals(step1(test,100), 8410)
  }

}
