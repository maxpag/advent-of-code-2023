package org.maxpagani
import scala.annotation.targetName

private def isEmptyRow( row: String ) : Boolean = {
  row.forall( _ == '.' )
}

private def isEmptyColumn( data: List[String], col: Int ) : Boolean = {
  data.forall( row => row(col) == '.' )
}

case class Galaxy( row: Int, column: Int )
{
  @targetName("compare")
  def <(other: Galaxy ) : Boolean = {
    row < other.row ||
      ( row == other.row && column < other.column )
  }
}

def distance( g1: Galaxy, g2: Galaxy ) : Int = {
  val dx = (g1.row - g2.row).abs
  val dy = (g1.column - g2.column).abs
  dx+dy
}

def findGalaxies(
  map: List[String],
) : List[Galaxy] = {
  map.zipWithIndex.flatMap( row => {
    val (rowString, rowNumber) = row
    rowString.zipWithIndex.flatMap( col => {
      val (character, colNumber) = col
      if ( character == '#' ) {
        Some( Galaxy( rowNumber, colNumber ))
      } else {
        None
      }
    } )
  })
}

def expand(coord: Int, value: List[Int], expansion: Int) : Int = {
  val spaces = value.count( _ < coord )
  coord + spaces*(expansion-1)
}

def expandSpace(
  galaxy: List[Galaxy],
  emptyRows: List[Int],
  emptyColumns: List[Int],
  expansion: Int = 2
) : List[Galaxy] = {
  for {
    g <- galaxy
    r = expand( g.row, emptyRows, expansion )
    c = expand( g.column, emptyColumns, expansion )
  } yield Galaxy( r, c )
}

def getEmptyRows( data: List[String] ) : List[Int] = {
  data.zipWithIndex.filter( row => isEmptyRow( row._1 ) ).map( _._2 )
}

def getEmptyColumns( data: List[String] ) : List[Int] = {
  (0 until data.head.length).filter( col => isEmptyColumn( data, col ) ).toList
}


def step1( data: List[String], expansion: Int = 2 ) : Long = {
  val emptyRows = getEmptyRows( data )
  val emptyColumns = getEmptyColumns( data )
  val observedGalaxies = findGalaxies( data )
  val galaxies = expandSpace( observedGalaxies, emptyRows, emptyColumns, expansion )
  val distances = for {
    g1 <- galaxies
    g2 <- galaxies
    if g1 < g2
  } yield distance( g1, g2 ).toLong
  distances.sum
}

def step2( data: List[String] ) : Long =
  step1( data, 1000000 )
