package org.maxpagani


case class Race( time: Long, record: Long ) {
  private def delta : Long = time*time - 4*record
  def count : Long = if delta < 0 then 0 else
    val lower = (time.toDouble - math.sqrt(delta))/2.0
    val upper = (time.toDouble + math.sqrt(delta))/2.0
    val lowerInt = lower.ceil.toLong
    val upperInt = upper.floor.toLong
    val invalidCount = (if lowerInt.toDouble == lower then 1 else 0) + (if upperInt.toDouble == upper then 1 else 0)
    upperInt - lowerInt - invalidCount + 1
}

case class Tournament(races: List[Race] )

def parse( data: List[String] ) : Tournament = {
  val times = data.head.split(" ").filter(_.nonEmpty).drop(1).map( _.toLong )
  val distances = data.tail.head.split( " ").filter(_.nonEmpty).drop(1).map( _.toLong )
  val races = times.zip(distances).map( (t,d) => Race(t,d))
  Tournament( races.toList )
}

def step1( data: List[String] ) : Long = {
  val input = parse( data )
  input.races.foldLeft( 1L )( (a,r) => {
      a * r.count
  } )
}

def step2( data: List[String] ) : Long = {
  val input = parse(data.map(_.filter(_ != ' ').map(c => if c == ':' then ' ' else c)))
  input.races.foldLeft(1L)((a, r) => {
    a * r.count
  })
}
