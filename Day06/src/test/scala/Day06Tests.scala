package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day06Tests extends munit.FunSuite {
  private val test = List(
    "Time:      7  15   30",
    "Distance:  9  40  200"
  )

  test("Day06 - Part1") {
    assertEquals(step1(test), 288)
  }

  test("Day06 - Part2") {
    assertEquals(step2(test), 30)
  }
}
