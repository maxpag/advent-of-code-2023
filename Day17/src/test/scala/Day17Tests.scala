package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day17Tests extends munit.FunSuite {
  private val test = List(
    "2413432311323",
    "3215453535623",
    "3255245654254",
    "3446585845452",
    "4546657867536",
    "1438598798454",
    "4457876987766",
    "3637877979653",
    "4654967986887",
    "4564679986453",
    "1224686865563",
    "2546548887735",
    "4322674655533"
  )

  test("Day17 - parse data") {
    val data = List( "123", "456", "789")
    val expected = Array( Array( 1, 4, 7), Array( 2, 5, 8), Array( 3, 6, 9))
    val a = parseData(data).map(_.mkString(" ")).mkString("\n")
    val b = expected.map(_.mkString(" ")).mkString("\n")
    assertEquals(a, b)
  }

  test("Day17 - Part1") {
    assertEquals(step1(test), 102L)
  }

  test("Day17 - Part2") {
    assertEquals(step2(test), ???)
  }
}
