package org.maxpagani

import scala.annotation.tailrec

enum Facing:
  case North
  case East
  case South
  case West

case class Position(x: Int, y: Int, f: Facing)
type Grid = Array[Array[Int]]
type HeatMap = Array[Array[Option[Int]]]

def nextStraight(p: Position): Position =
  p.f match {
    case Facing.North => p.copy(y = p.y - 1)
    case Facing.East => p.copy(x = p.x + 1)
    case Facing.South => p.copy(y = p.y + 1)
    case Facing.West => p.copy(x = p.x - 1)
  }

def splitHorizontal(p: Position): List[Position] =
  List(p.copy(f = Facing.East), p.copy(f = Facing.West))

def splitVertical(p: Position): List[Position] =
  List(p.copy(f = Facing.North), p.copy(f = Facing.South))

def isOnGrid(g: Grid, p: Position): Boolean =
  val w = g.length
  val h = g.head.length
  p.x >= 0 && p.x < w && p.y >= 0 && p.y < h

def split( p: Position) : List[Position] =
  p.f match {
    case Facing.North | Facing.South => splitHorizontal(p)
    case Facing.East | Facing.West => splitVertical(p)
  }

case class Walker(p: Position, heat: Int)

def generateNextOne(walker: Walker, grid: Grid, heatMap: HeatMap): Option[Walker] =
  val p1 = nextStraight(walker.p)
  Some(p1)
    .filter( p => isOnGrid(grid, p) )
    .map( p => Walker(p, grid(p1.x)(p1.y)+walker.heat ))
//    .filter( w => heatMap(w.p.x)(w.p.y).isEmpty || heatMap(w.p.x)(w.p.y).get > w.heat)

def generateNext(walker: Walker, grid: Grid, heatMap: HeatMap): List[Walker] =
  val next1 = generateNextOne(walker, grid, heatMap)
  val next2 = next1.flatMap( w => generateNextOne( w, grid, heatMap ))
  val next3 = next2.flatMap( w => generateNextOne( w, grid, heatMap ))
  val walkers : List[Walker] = List(next1.toList, next2.toList, next3.toList).flatten
  walkers

def isArrived(p: Position, grid: Grid): Boolean =
  val w = grid.length
  val h = grid.head.length
  p.x == w-1 && p.y == h-1

def reduce( walkers: Map[Position,Int], news: List[Walker] ): Map[Position,Int] =
  news.foldLeft( walkers )(
    (acc, w) => acc.updatedWith(w.p)( _.map( _ min w.heat ).orElse( Some(w.heat) ) )
  )


def countHeat(grid: Grid): Int =
  val w = grid.length
  val h = grid.head.length
  val heatMap: Array[Array[Option[Int]]] = Array.ofDim[Option[Int]](w, h)
  heatMap.indices.foreach(
    x => heatMap(x).indices.foreach(
      y => heatMap(x)(y) = None
    )
  )

  var counter = 0L

  @tailrec
  def loop(walkers: Map[Position,Int], heat: Option[Int]): Int =
    counter += 1
    if counter % 100000 == 0 then
      val mapped = heatMap.flatten.count( _.isDefined )
      println(s"Mapped ${mapped} positions, front: ${walkers.size}, heat: ${heat}")
    if walkers.isEmpty then
      heat.get
    else
      val first : (Position,Int) = walkers.head
      val w = Walker( first._1, first._2 )
      val ws = walkers.removed(first._1)
      if isArrived( w.p, grid ) then
        loop(ws, Some(heat.getOrElse(w.heat) min w.heat))
      else if heatMap(w.p.x)(w.p.y).isEmpty || heatMap(w.p.x)(w.p.y).get > w.heat then
        heatMap(w.p.x)(w.p.y) = Some(w.heat)
        val next = split(w.p).flatMap( p => generateNext( w.copy(p=p), grid, heatMap))
        loop(reduce(ws, next), heat)
      else
        loop(ws , heat)

  val initialSet = List(
    generateNext(Walker(Position(0, 0, Facing.East), 0),grid,heatMap),
    generateNext(Walker(Position(0, 0, Facing.South), 0),grid,heatMap)
  ).flatten.map( w => w.p -> w.heat ).toMap
  heatMap(0)(0) = Some(grid(0)(0))
  loop( initialSet, None)


def parseData(data: List[String]): Array[Array[Int]] =
  val w = data.head.length
  val h = data.length
  val grid: Array[Array[Int]] = Array.ofDim[Int](w, h)
  for( x <- grid.indices; y <- grid(x).indices ) grid(x)(y) = data(y)(x).toInt - '0'.toInt
  grid

def step1( data: List[String] ) : Long =
  val grid = parseData(data)
  countHeat(grid)-grid(0)(0)

def step2( data: List[String] ) : Long = ???
