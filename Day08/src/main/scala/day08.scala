package org.maxpagani

import scala.annotation.tailrec
import math.Integral.Implicits.infixIntegralOps

enum Move {
  case Left
  case Right
}

case class Navigation( s: String, n: Int = 0) {
  def getMove : Move = s.charAt(n) match {
    case 'L' => Move.Left
    case 'R' => Move.Right
  }
  def next : Navigation = Navigation( s, (this.n + 1)%s.length )
}

case class Node( label: String, leftLabel: String, rightLabel: String ) {
  def getLabel : String = label
  def get( side: Move ) : String = side match {
    case Move.Left => leftLabel
    case Move.Right => rightLabel
  }

  def isEnding : Boolean = label.last == 'Z'
  def isStarting : Boolean = label.last == 'A'
}

type FullMap = Map[String, Node]

def countStepFromTo(start: String, isEnding: String => Boolean, navigation: Navigation, fullMap: FullMap ) : Long = {
  @tailrec
  def countStepTo( current: String, navigation: Navigation, count: Long ) : Long = {
    if (isEnding( current )) count
    else {
      val node = fullMap(current)
      countStepTo( node.get(navigation.getMove), navigation.next, count+1 )
    }
  }

  countStepTo( start, navigation, 0 )
}


// From Rosetta Code: https://rosettacode.org/wiki/Least_common_multiple#Scala
def gcd[N: Integral](a: N, b: N): N = if (b == 0) a.abs else gcd(b, a % b)
def lcm[N: Integral](a: N, b: N) = (a * b).abs / gcd(a, b)

def lcm[N: Integral]( l: List[N]) : N = l.foldLeft(Integral[N].fromInt(1))(lcm(_, _))

def countThreadSteps(
  threads: List[String],
  isEnding: String => Boolean,
  navigation: Navigation,
  fullMap: FullMap
): Long =
  val loopCounts = threads.map( countStepFromTo(_, isEnding, navigation, fullMap) )
  lcm(loopCounts)

def parse( data: List[String] ) : (Navigation,FullMap) =
  (
    Navigation(data.head),
    data.drop(2).map( line => {
      val s"$label = ($leftLabel, $rightLabel)" = line
      (label, Node(label, leftLabel, rightLabel))
    }).toMap
  )

def step1( data: List[String] ) : Long =
  val (navigation, fullMap) = parse(data)
  countStepFromTo("AAA", _ == "ZZZ", navigation, fullMap)


def step2( data: List[String] ) : Long =
  val (navigation, fullMap) = parse(data)
  val threads = fullMap.values.filter( _.isStarting ).map( _.label ).toList
  countThreadSteps(threads, _.last == 'Z', navigation, fullMap)