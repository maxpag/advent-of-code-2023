package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day08Tests extends munit.FunSuite {
  private val test1 = List(
    "RL",
    "",
    "AAA = (BBB, CCC)",
    "BBB = (DDD, EEE)",
    "CCC = (ZZZ, GGG)",
    "DDD = (DDD, DDD)",
    "EEE = (EEE, EEE)",
    "GGG = (GGG, GGG)",
    "ZZZ = (ZZZ, ZZZ)"
  )

  private val test2 = List(
    "LLR",
    "",
    "AAA = (BBB, BBB)",
    "BBB = (AAA, ZZZ)",
    "ZZZ = (ZZZ, ZZZ)"
  )

  private val test3 = List(
    "LR",
    "",
    "11A = (11B, XXX)",
    "11B = (XXX, 11Z)",
    "11Z = (11B, XXX)",
    "22A = (22B, XXX)",
    "22B = (22C, 22C)",
    "22C = (22Z, 22Z)",
    "22Z = (22B, 22B)",
    "XXX = (XXX, XXX)"
  )

  test( "Day08 - parse" ) {
    val (navigation,map) = parse(test1)
    assertEquals(navigation, Navigation("RL"))
    assertEquals(map, Map(
      "AAA" -> Node( "AAA", "BBB", "CCC"),
      "BBB" -> Node( "BBB", "DDD", "EEE"),
      "CCC" -> Node( "CCC", "ZZZ", "GGG"),
      "DDD" -> Node( "DDD", "DDD", "DDD"),
      "EEE" -> Node( "EEE", "EEE", "EEE"),
      "GGG" -> Node( "GGG", "GGG", "GGG"),
      "ZZZ" -> Node( "ZZZ", "ZZZ", "ZZZ")
    ))
  }

  test("Day08 - Part1") {
    assertEquals(step1(test1), 2L)
    assertEquals(step1(test2), 6L)
  }

  test("Day08 - Part2") {
    assertEquals(step2(test3), 6L)
  }
}
