package org.maxpagani

import munit.*
import org.maxpagani.Comparison.Less
import org.maxpagani.step1
import org.maxpagani.step2

class Day19Tests extends munit.FunSuite {
  private val test = List(
    "px{a<2006:qkq,m>2090:A,rfg}",
    "pv{a>1716:R,A}",
    "lnx{m>1548:A,A}",
    "rfg{s<537:gd,x>2440:R,A}",
    "qs{s>3448:A,lnx}",
    "qkq{x<1416:A,crn}",
    "crn{x>2662:A,R}",
    "in{s<1351:px,qqz}",
    "qqz{s>2770:qs,m<1801:hdj,R}",
    "gd{a>3333:R,R}",
    "hdj{m>838:A,pv}",
    "",
    "{x=787,m=2655,a=1222,s=2876}",
    "{x=1679,m=44,a=2067,s=496}",
    "{x=2036,m=264,a=79,s=2244}",
    "{x=2461,m=1339,a=466,s=291}",
    "{x=2127,m=1623,a=2188,s=1013}"
  )

  test("Day19 - parseSwitcher") {
    val texts=List(
      "a>1716:R",
      "x<1716:A",
      "m>1716:xpq",
      "foo"
    )

    val expected=List(
      Switcher(Some(Selector(Ratings.A,Comparison.Greater,1716)),Rejected),
      Switcher(Some(Selector(Ratings.X,Comparison.Less,1716)),Accepted),
      Switcher(Some(Selector(Ratings.M,Comparison.Greater,1716)),DestinationQueue("xpq")),
      Switcher(None,DestinationQueue("foo"))
    )

    texts zip expected foreach {
      case (text,expected) => assertEquals(parseSwitcher(text),expected)
    }
  }

  test("Day19 - parseWorkflow") {
    val text = "px{a<2006:qkq,m>2090:A,rfg}"
    val workflow = parseWorkflow(text)
    assertEquals(workflow.label, "px" )
    assertEquals(workflow.switchers, List(
      Switcher(Some(Selector(Ratings.A,Comparison.Less,2006)),DestinationQueue("qkq")),
      Switcher(Some(Selector(Ratings.M,Comparison.Greater,2090)),Accepted),
      Switcher(None,DestinationQueue("rfg"))
    ))
  }

  test("Day19 - switcher") {
    val text = "px{a<2006:qkq,m>2090:A,rfg}"
    val workflow = parseWorkflow(text)
    val vector = List(
      (Part( x=100, m=20, a=30, s=40), DestinationQueue("qkq")),
      (Part( x=100, m=2091, a=2100, s=40), Accepted),
      (Part( x=100, m=2089, a=2100, s=40), DestinationQueue("rfg"))
    )
    vector.foreach(
      (part,expected) => assertEquals(workflow.getDestinationFor(part),expected)
    )
  }

  test("Day19 - Part1") {
    assertEquals(step1(test), 19114L)
  }

  // part 2
  // px{a<2006:qkq,m>2090:A,rfg}
  //   x = D, m = D, a = 1..2005, s = D -> qkq
  //   x = D, m = 1..2090, a = 2006..4000, s = D -> rfg
  //   x = D, m = 2091..4000, a = 2006..4000, s = D -> A
  // so, this rule accepts D*D*(4000-2091+)(4000-2006+1)

  test("Day19 - Part2") {
    assertEquals(step2(test), 167409079868000L)
  }
}
