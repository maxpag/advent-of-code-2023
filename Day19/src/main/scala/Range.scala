package org.maxpagani

object Range {
  final val Universe = Range( 1, 4000 )
}


case class Range( start: Int, count: Int ) {
  def end = start+count
  def lastValue = end-1

  def intersect(other: Range): Option[Range] =
    val newStart = start max other.start
    val newEnd = end min other.end
    if newEnd < newStart then None
    else Some( Range( newStart, newEnd-newStart ))

  def union(other: Range): List[Range] =
    if start > other.end || end < other.start then List(this, other)
    else List(Range(start.min(other.start), end.max(other.end)))

  def isEmpty : Boolean = count == 0
  def isNonEmpty : Boolean = count != 0

  def complement : List[Range] =
    import Range.Universe
    assert( start >= Universe.start )
    assert( end < Universe.end )
    val firstStart = Universe.start
    val firstCount = start-Universe.start
    val secondStart = end
    val secondCount = Universe.end-end
    List( Range( firstStart, firstCount), Range(secondStart, secondCount ))
      .filter( _.isNonEmpty )

  def minus(other: Range): List[Range] =
    other.complement.flatMap( intersect )

  def contains( n: Int ): Boolean = n >= start && n < end
}
