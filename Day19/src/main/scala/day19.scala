package org.maxpagani

import scala.annotation.tailrec

case class Part( x: Int, m: Int, a: Int, s: Int ):
  def sum = x + m + a + s


enum Ratings:
  case X, M, A, S

object Ratings:
  def fromName(str: String): Ratings =
    str match {
      case "x" => X
      case "m" => M
      case "a" => A
      case "s" => S
    }

enum Comparison:
  case Greater, Less

case class Selector(rating: Ratings, comparison: Comparison, value: Int ) {
  def isPartSelected(p: Part ) : Boolean = {
    val compFn = comparison match {
      case Comparison.Greater => (x: Int, y: Int) => x > y
      case Comparison.Less => (x: Int, y: Int) => x < y
    }
    rating match {
      case Ratings.X => compFn( p.x, value )
      case Ratings.M => compFn( p.m, value )
      case Ratings.A => compFn( p.a, value )
      case Ratings.S => compFn( p.s, value )
    }
  }

  case class Partition( accept: Option[Range], reject: Option[Range] )
  def partition( range: Range ) : Partition = {
    comparison match {
      case Comparison.Greater =>
        if range.contains(value) then
          val firstUpper = value+1
          Partition(
            Some( Range(firstUpper,range.end-firstUpper)),
            Some( Range(range.start,firstUpper-range.start))
          )
        else if range.start > value then
          Partition( Some( range), None )
        else
          Partition( None, Some( range ))

      case Comparison.Less =>
        if range.contains(value) then
          Partition(
            Some(Range(range.start, value - range.start)),
            Some(Range(value, range.end - value))
          )
        else if range.end <= value then
          Partition( Some( range), None )
        else
          Partition( None, Some( range ))
    }
  }
}



sealed trait Destination
case class DestinationQueue( queue: String ) extends Destination
case object Accepted extends Destination
case object Rejected extends Destination

object Destination:
  def fromName( str: String ) : Destination =
    str match {
      case "A" => Accepted
      case "R" => Rejected
      case queue => DestinationQueue( queue )
    }

case class Switcher(rule: Option[Selector], destination: Destination ) {
  def getDestinationFor( part: Part ) : Option[Destination] =
    rule match {
      case Some( check ) =>
        if check.isPartSelected( part ) then Some(destination) else None
      case None => Some(destination)
    }
}

case class Workflow( label: String, switchers: List[Switcher] ) {
  def getDestinationFor( part: Part ) : Destination =
    @tailrec
    def getDestinationFor( switchers: List[Switcher] ) : Destination =
      switchers match {
        case Nil => Rejected
        case head :: tail =>
          head.getDestinationFor( part ) match {
            case Some(destination) => destination
            case None => getDestinationFor( tail )
          }
      }

    getDestinationFor( switchers )
}

def parsePart( part: String ) : Part =
  val s"{x=$x,m=$m,a=$a,s=$s}" = part
  Part( x.toInt, m.toInt, a.toInt, s.toInt )

def makeCheck( rating: String, comparison: Comparison, value: String ) : Selector =
  Selector( Ratings.fromName( rating ), comparison, value.toInt )

def parseSwitcher(str: String) : Switcher = {
  val data = (str match {
    case s"$rating>$value:$dest" => (Some(makeCheck( rating, Comparison.Greater, value )), dest )
    case s"$rating<$value:$dest" => (Some(makeCheck( rating, Comparison.Less, value )), dest )
    case s"$dest" => (None, dest)
  })
  Switcher( data(0), Destination.fromName(data(1)) )
}

def applySwitcher( part: Part, switcher: Switcher ) : Destination =
  switcher.rule match {
    case Some( check ) =>
      if check.rating match {
        case Ratings.X => check.value > part.x
        case Ratings.M => check.value > part.m
        case Ratings.A => check.value > part.a
        case Ratings.S => check.value > part.s
      } then switcher.destination else Rejected
    case None => switcher.destination
  }

def parseWorkflow(pipeline: String ) : Workflow =
  val label = pipeline.takeWhile( _ != '{' )
  val rulesText = pipeline.dropWhile(_ != '{').drop(1).dropRight(1)
  val rulesData = rulesText.split( ',' )
  val rules = rulesData.map( parseSwitcher ).toList
  Workflow( label, rules )

def sortPart(part: Part, queues: Map[String, Workflow]) : Destination = {
  @tailrec
  def sortPart( part: Part, workflow: Workflow ) : Destination =
    val dest = workflow.getDestinationFor( part )
    dest match {
      case Accepted => Accepted
      case Rejected => Rejected
      case DestinationQueue( queue ) => sortPart( part, queues(queue) )
    }

  sortPart( part, queues("in") )
}


def step1( data: List[String] ) : Long =
  val (rulesData,partsData) = data.splitAt( data.indexOf( "" ) )
  val pipeline = rulesData
    .map( parseWorkflow )
    .map( w => (w.label, w) )
    .toMap
  // drop the empty line
  val parts = partsData.drop(1).map( parsePart )

  parts.map( part => (part,sortPart( part, pipeline )))
    .filter( _._2 == Accepted )
    .map( _._1.sum.toLong )
    .sum


def step2( data: List[String] ) : Long = ???
