package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day21Tests extends munit.FunSuite {
  private val test = List(
    "...........",
    ".....###.#.",
    ".###.##..#.",
    "..#.#...#..",
    "....#.#....",
    ".##..S####.",
    ".##..#...#.",
    ".......##..",
    ".##.#.####.",
    ".##..##.##.",
    "..........."
  )


  test("Day21 - Part1") {
    val terrain = parse(test)
    val startCandidate = terrain.find(_ == 'S')
    val count = startCandidate match
      case None => 0
      case Some(start) =>
        val iterations = 6
        countPositions(terrain, start, iterations)

    assertEquals(count, 16L)
  }

  test("Day21 - Part2") {
    assertEquals(step2(test), ???)
  }
}
