package org.maxpagani

case class Position( x: Int, y: Int )

def nextPosition( p: Position, f: Facing ) : Position =
  f match
    case Facing.East => Position( p.x+1, p.y )
    case Facing.North => Position( p.x, p.y-1 )
    case Facing.South => Position( p.x, p.y+1 )
    case Facing.West => Position( p.x-1, p.y )

def adjacents( p: Position ) : List[Position] =
  List( Facing.East, Facing.North, Facing.South, Facing.West )
    .map( nextPosition( p, _ ) )