package org.maxpagani

import scala.annotation.tailrec
import scala.collection.ArrayOps
import scala.collection.immutable.HashSet

type Generate[A] = (Position,Grid[A]) => List[Position]
type IsGoal[A] = (Position,Grid[A]) => Boolean
type Prune[A] = (List[Position],Grid[A]) => List[Position]

def walk[A](
  p: Position,
  terrain: Grid[A],
  generate: Generate[A],
  prune: Prune[A],
  iterations: Int,
  state:HashSet[Position]
) : HashSet[Position] = {
  @tailrec
  def walkImpl(ps: HashSet[Position], iteration: Int): HashSet[Position] =
    if iteration == 0 then ps
    else if ps.isEmpty then assert(false, s"No more positions to walk (iterations left=$iteration)")
    else
      val next = ps.flatMap( p => {
        val allSteps = generate(p, terrain)
        val newSteps = allSteps.flatMap(terrain.validate)
        val prunedSteps = prune(newSteps, terrain)
        prunedSteps
      })
      if iteration % 1000 == 0 then println(s"iteration=$iteration, size=${next.size}")
      walkImpl(next, iteration-1)

  walkImpl(HashSet(p), iterations)
}

def rowToString( stateRow: Array[Boolean], terrainRow: Array[Char]) : String = {
  (stateRow zip terrainRow map ((s: Boolean,t: Char) => (s,t) match {
    case (true,'.') => 'O'
    case (_,'S') => 'S'
    case (true,'#') => '!'
    case (true,c)   => '?'
    case (false,c)  => c
  })).mkString
}

def toString( state: HashSet[Position], terrain: SimpleGrid[Char]) : String = {
  val stateGrid = SimpleGrid[Boolean](terrain.width, terrain.height, false)
  state.foreach( p => stateGrid.setAt(p, true))
  (stateGrid.grid.toList zip terrain.grid.toList map {
    (stateRow:Array[Boolean],terrainRow:Array[Char]) =>
      rowToString(stateRow, terrainRow)
  }).mkString("\n")
}

def countPositions( terrain: Terrain, start: Position, iterations: Int ) : Long =
  val generate : Generate[Char] = (p,g) => adjacents(p).flatMap( g.validate ).filter( p => g.getAt(p) != '#')
  val prune : Prune[Char] = (ps,g) => ps  // no need to prune, thanks to the HashSet
  val pathState = walk( start, terrain, generate, prune, iterations, HashSet.empty )
  pathState.size

def parse( data: List[String]) : SimpleGrid[Char] =
  SimpleGrid( data, (c:Char) => c )

def parseTorus(data: List[String]): TorusGrid[Char] =
  TorusGrid(data, (c: Char) => c)

type Terrain = Grid[Char]
type PathState = Grid[Boolean]

def step1( data: List[String] ) : Long =
  val terrain = parse(data)
  val startCandidate = terrain.find( _ == 'S' )
  startCandidate match
    case None => 0
    case Some(start) =>
      val iterations = 64
      countPositions( terrain, start, iterations )

// I think that for step 2 you need to find the frontier of expansion for
// the given size and then remove each position that is on a square that
// can't be occupied at iteration n % 2.
def step2( data: List[String] ) : Long =
  val terrain = parseTorus(data)
  val startCandidate = terrain.find(_ == 'S')
  startCandidate match
    case None => 0
    case Some(start) =>
      val iterations = 26501365
      countPositions(terrain, start, iterations)
