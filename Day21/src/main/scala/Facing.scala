package org.maxpagani

enum Facing:
  case North, East, South, West
