package org.maxpagani

import scala.reflect.ClassTag

object SimpleGrid
{
  def apply[A: ClassTag]( width: Int, height: Int, emptyValue: A ) : SimpleGrid[A] =
    val theGrid: Array[Array[A]] = Array.ofDim[A](height, width)
    theGrid.indices.foreach(
      y => theGrid(y).indices.foreach(
        x => theGrid(y)(x) = emptyValue
      )
    )
    new SimpleGrid(theGrid)
    
  def apply[A: ClassTag]( data: List[String], convert: Char => A ) : SimpleGrid[A] =
    val theGrid: Array[Array[A]] = Array.ofDim[A](data.length, data.head.length)
    data.indices.foreach(
      y => data(y).indices.foreach(
        x => theGrid(y)(x) = convert(data(y)(x))
      )
    )
    new SimpleGrid(theGrid)
}

trait Grid[A]:
  def isValidPosition( p: Position ) : Boolean
  def validate( p: Position ) : Option[Position]
  def getAt( p: Position ) : A
  def setAt( p: Position, a: A ) : Unit


class SimpleGrid[A]( val grid : Array[Array[A]] ) extends Grid[A] :
  val width: Int = grid.head.length
  val height: Int = grid.length
  def isValidPosition( p: Position ): Boolean =
    p.x >= 0 && p.x < width &&
      p.y >= 0 && p.y < height

  def validate( p: Position ) : Option[Position] =
    Some(p).filter(isValidPosition)

  def getAt( p: Position ) : A =
    grid(p.y)(p.x)

  def setAt( p: Position, a: A ) : Unit =
    grid(p.y)(p.x) = a

  def count( p: A => Boolean ) : Int =
    grid.map( _.count(p) ).sum
    
  def find( predicate: A => Boolean ) : Option[Position] =
    grid.indices.flatMap(
      y => grid(y).indices.map(
        x => Position(x,y)
      ).filter(
        p => predicate( getAt(p) )
      )
    ).headOption

object TorusGrid {
  def apply[A: ClassTag](width: Int, height: Int, emptyValue: A): TorusGrid[A] =
    val theGrid: Array[Array[A]] = Array.ofDim[A](height, width)
    theGrid.indices.foreach(
      y => theGrid(y).indices.foreach(
        x => theGrid(y)(x) = emptyValue
      )
    )
    new TorusGrid(theGrid)

  def apply[A: ClassTag](data: List[String], convert: Char => A): TorusGrid[A] =
    val theGrid: Array[Array[A]] = Array.ofDim[A](data.length, data.head.length)
    data.indices.foreach(
      y => data(y).indices.foreach(
        x => theGrid(y)(x) = convert(data(y)(x))
      )
    )
    new TorusGrid(theGrid)
}

class TorusGrid[A]( val grid : Array[Array[A]] ) extends Grid[A] :
  private val width: Int = grid.head.length
  private val height: Int = grid.length
  def isValidPosition( p: Position ): Boolean = true

  def validate( p: Position ) : Option[Position] = Some(p)

  def normalize( p: Position ) : Position =
    val baseX = p.x % width
    val baseY = p.y % height
    Position(
      if baseX < 0 then baseX + width else baseX,
      if baseY < 0 then baseY + height else baseY
    )

  def getAt( p: Position ) : A =
    val baseP = normalize(p)
    grid(baseP.y)(baseP.x)

  def setAt( p: Position, a: A ) : Unit =
    val baseP = normalize(p)
    grid(baseP.y)(baseP.x) = a

  def count( p: A => Boolean ) : Int =
    grid.map( _.count(p) ).sum

  def find( predicate: A => Boolean ) : Option[Position] =
    grid.indices.flatMap(
      y => grid(y).indices.map(
        x => Position(x,y)
      ).filter(
        p => predicate( getAt(p) )
      )
    ).headOption
