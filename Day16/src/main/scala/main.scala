package org.maxpagani

import scala.io.Source
import org.maxpagani.step1

@main
def main(): Unit = {
  val filename = "data.in"
  val source = Source.fromFile(filename)
  val lines = source.getLines().toList
  source.close()

  println(step1(lines))
  println(step2(lines))
}