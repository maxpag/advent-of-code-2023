package org.maxpagani

import scala.annotation.tailrec

enum Facing:
  case North
  case East
  case South
  case West

case class Position( x: Int, y: Int, f: Facing )

def nextStraight(p: Position ) : Position =
  p.f match {
    case Facing.North => p.copy( y = p.y - 1 )
    case Facing.East => p.copy( x = p.x + 1 )
    case Facing.South => p.copy( y = p.y + 1 )
    case Facing.West => p.copy( x = p.x - 1 )
  }

def isFacingHorizontal( f: Facing ): Boolean =
  f == Facing.East || f == Facing.West

def isFacingVertical( f: Facing ): Boolean =
  f == Facing.North || f == Facing.South

def splitHorizontal( p: Position ): List[Position] =
  List( p.copy( f = Facing.East ), p.copy( f = Facing.West ))

def splitVertical( p: Position ): List[Position] =
  List( p.copy( f = Facing.North ), p.copy( f = Facing.South ))

def reflect45( p: Position ): Position =
  p.f match {
    case Facing.North => p.copy( f = Facing.East )
    case Facing.East => p.copy( f = Facing.North )
    case Facing.South => p.copy( f = Facing.West )
    case Facing.West => p.copy( f = Facing.South )
  }

def reflect135( p: Position ): Position =
  p.f match {
    case Facing.North => p.copy( f = Facing.West )
    case Facing.East => p.copy( f = Facing.South )
    case Facing.South => p.copy( f = Facing.East )
    case Facing.West => p.copy( f = Facing.North )
  }

def advance( p: Position, c: Char ) : List[Position] =
  c match {
    case '.' => List( nextStraight( p ))
    case '-' if isFacingHorizontal( p.f ) => List( nextStraight( p))
    case '-' => splitHorizontal( p ).map( nextStraight )
    case '|' if isFacingVertical( p.f ) => List( nextStraight( p))
    case '|' => splitVertical( p ).map( nextStraight )
    case '/' => List(nextStraight(reflect45( p )))
    case '\\' => List(nextStraight(reflect135( p )))
  }

def isOnGrid( w: Int, h: Int, p: Position ) : Boolean =
  p.x >= 0 && p.x < w && p.y >= 0 && p.y < h

case class GridCellContent( content: Set[Facing] )

def countEnergy( data: List[String], start: Position ) : Int =
  val w = data.head.length
  val h = data.length
  val grid: Array[Array[GridCellContent]] = Array.ofDim[GridCellContent](w, h)
  grid.indices.foreach(
    x => grid(x).indices.foreach(
      y => grid(x)(y) = GridCellContent(Set.empty)
    )
  )

  @tailrec
  def loop(positions: List[Position], count: Int): Int =
    positions match {
      case Nil =>
        count
      case p :: ps =>
        val inc = if grid(p.x)(p.y).content.isEmpty then 1 else 0
        val next =
          if (grid(p.x)(p.y).content.contains(p.f)) then
            List.empty
          else
            grid(p.x)(p.y) = GridCellContent(grid(p.x)(p.y).content + p.f)
            val next = advance(p, data(p.y)(p.x))
            next.filter(isOnGrid(w, h, _))
        loop(ps ++ next, count + inc)
    }

  loop(List(start), 0)

def step1( data: List[String] ) : Long =
  val start = Position(0, 0, Facing.East)
  countEnergy(data, start)

def step2( data: List[String] ) : Long =
  val w = data.head.length
  val h = data.length
  val testH = (0 until w).flatMap( x => IndexedSeq(Position(x, 0, Facing.South), Position(x, h-1, Facing.North)))
  val testV = (0 until h).flatMap( y => IndexedSeq(Position(0, y, Facing.East), Position(w-1, y, Facing.West)))

  (testH ++ testV)
    .map( p => countEnergy(data, p))
    .max