package org.maxpagani

import org.maxpagani.{step1, step2}
import munit._
import munit.Clue.generate

class Day16Tests extends munit.FunSuite {
  private val test = List(
    ".|...\\....",
    "|.-.\\.....",
    ".....|-...",
    "........|.",
    "..........",
    ".........\\",
    "..../.\\\\..",
    ".-.-/..|..",
    ".|....-|.\\",
    "..//.|...."
  )

  test("Day07 - Part1") {
    assertEquals(step1(test), 46L)
  }

  test("Day07 - Part2") {
    assertEquals(step2(test), 51L)
  }
}
