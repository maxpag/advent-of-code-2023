package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day20Tests extends munit.FunSuite {
  private val test = List(
    "broadcaster -> a, b, c",
    "%a -> b",
    "%b -> c",
    "%c -> inv",
    "&inv -> a"
  )


  test("Day20 - Part1") {
    assertEquals(step1(test), 32000000L)
  }

  test("Day20 - Part2") {
    assertEquals(step2(test), ???)
  }
}
