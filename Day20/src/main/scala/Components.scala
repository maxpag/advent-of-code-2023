package org.maxpagani

import scala.collection.mutable

trait Component( val label: String, val outputs: List[ComponentLabel] ):
  protected def outputPulse( pulse: Pulse ): List[Signal] = {
    outputs.map( Signal( label, pulse, _ ))
  }
  def pulse( source: ComponentLabel, pulse: Pulse ): List[Signal]
  def addInput( input: ComponentLabel ) : Component

case class FlipFlop(
  override val label: String,
  override val outputs: List[ComponentLabel]
)
  extends Component( label, outputs ):
  enum State:
    case Off
    case On

  var state: State = State.Off

  override def pulse(source: ComponentLabel, pulse: Pulse): List[Signal] =
    (pulse,state) match {
      case (Pulse.High, _) =>
        List.empty
      case (Pulse.Low, State.Off) =>
        state = State.On
        outputPulse( Pulse.High )
      case (Pulse.Low, State.On) =>
        state = State.Off
        outputPulse( Pulse.Low )
      case _ => List.empty
    }

  override def addInput(input: ComponentLabel): Component = this


case class Broadcaster(
  override val outputs: List[ComponentLabel]
) extends Component( "broadcaster", outputs ):
  override def pulse(source: ComponentLabel, pulse: Pulse): List[Signal] =
    outputPulse( pulse )

  override def addInput(input: ComponentLabel): Component = ???

case class Conjunction(
  override val label: String,
  override val outputs: List[ComponentLabel],
  var inputs : mutable.HashMap[ComponentLabel, Pulse] = mutable.HashMap.empty
) extends Component( label, outputs ):
  override def pulse(source: ComponentLabel, pulse: Pulse): List[Signal] = {
    inputs(source) = pulse
    if (inputs.values.forall( _ == Pulse.High )) {
      outputPulse( Pulse.Low )
    } else {
      outputPulse( Pulse.High )
    }
  }

  override def addInput(input: ComponentLabel): Component =
    this.copy( inputs = inputs.addOne( input, Pulse.Low ) )

case class TestPoint( override val label: String ) extends Component( label, List.empty ):
  override def pulse(source: ComponentLabel, pulse: Pulse): List[Signal] =
    List.empty

  override def addInput(input: ComponentLabel): Component = this