package org.maxpagani

enum Pulse:
  case Low
  case High

case class Signal( source: ComponentLabel, pulse: Pulse, destination: ComponentLabel )

