package org.maxpagani

import scala.annotation.tailrec
import scala.collection.immutable.HashMap
import scala.collection.mutable

object ComponentMap extends scala.collection.mutable.HashMap[ComponentLabel, Component]

def stringToOutputs( outputs: String ): List[ComponentLabel] =
  outputs.split( "," ).toList.map( _.trim )

def lineToComponent( line: String ): Component =
  line match {
    case s"broadcaster -> $outputs" => Broadcaster( stringToOutputs( outputs ) )
    case s"%$label -> $outputs" => FlipFlop( label, stringToOutputs( outputs ) )
    case s"&$label -> $outputs" => Conjunction( label, stringToOutputs( outputs ) )
  }

def parse( data: List[String] ) : Map[ComponentLabel, Component] =
  val components = data.map( lineToComponent ).map( c => (c.label, c) ).toMap
  // connects outputs to inputs
  val connections = components.map( (label,component) => (label,component.outputs)  )

  connections.foldLeft( components )( (components, connection) =>
    val fromComponent = connection._1
    connection._2.foldLeft( components )((components, toComponent) =>
      components.updatedWith(toComponent)( c => c.map( _.addInput( fromComponent )))
    )
  )


def step1( data: List[String] ) : Long =
  val circuit = parse( data )
  val trigger = Signal( "button", Pulse.Low, "broadcaster" )
  val result = (1 to 1000).foldLeft( SimulationResult(0,0) )(
    (counters, _) =>
      val result = runSimulation( trigger, circuit )
      SimulationResult( counters.lowCount + result.lowCount, counters.highCount + result.highCount )
  )
  result.lowCount * result.highCount

def step2( data: List[String] ) : Long =
  val circuit = parse(data) + ( "rx" -> TestPoint( "rx" ))
  val trigger = Signal("button", Pulse.Low, "broadcaster")
  repeatSimulationUntil(
    trigger,
    circuit,
    _.exists( s => s.destination == "rx" && s.pulse == Pulse.Low )
  )
