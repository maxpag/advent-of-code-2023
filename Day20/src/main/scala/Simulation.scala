package org.maxpagani

import scala.annotation.tailrec

def simulate( signals: List[Signal], circuit: Map[ComponentLabel, Component] ): List[Signal] =
  signals.flatMap( s => {
    val destination = circuit.get(s.destination)
    destination match
      case Some( component ) => component.pulse( s.source, s.pulse )
      case None => List.empty
  })

case class SimulationResult( lowCount: Long, highCount : Long )

def runSimulation( trigger: Signal, circuit: Map[ComponentLabel, Component] ): SimulationResult =

  @tailrec
  def simulation( signals: List[Signal], counters: SimulationResult ): SimulationResult =
    if signals.isEmpty then
      counters
    else
      val nextSignals = simulate( signals, circuit)
      val nextCounts = SimulationResult(
        counters.lowCount + nextSignals.count( _.pulse == Pulse.Low ),
        counters.highCount + nextSignals.count( _.pulse == Pulse.High )
      )
      simulation( nextSignals, nextCounts )

  simulation(
    List( trigger ),
    if trigger.pulse == Pulse.Low then SimulationResult( 1, 0 ) else SimulationResult( 0, 1 )
  )

def repeatSimulationUntil( trigger: Signal, circuit: Map[ComponentLabel, Component], predicate: List[Signal] => Boolean ): Long = {

  @tailrec
  def simulation(signals: List[Signal]): Boolean =
    if signals.isEmpty then
      false
    else if( predicate( signals ) ) then
      true
    else
      val nextSignals = simulate(signals, circuit)
      simulation( nextSignals )

  @tailrec
  def countButtonPresses( count: Long ) : Long =
    val signals = List( trigger )
    if simulation( signals ) then
      count
    else
      if( count % 1000 == 0 ) then
        println( s"Count: $count" )
      countButtonPresses( count + 1 )

  countButtonPresses( 0 )
}

