package org.maxpagani
import scala.io.Source
import org.maxpagani.step1
import org.maxpagani.step2

@main
def main(): Unit = {
  val filename = "data.in"
  val source = Source.fromFile(filename)
  val lines = source.getLines().toList
  source.close()
  println(s"step1: ${step1(lines)}")
  println(s"step2: ${step2(lines)}")

}
