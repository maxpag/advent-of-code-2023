package org.maxpagani

import scala.annotation.tailrec

def decode1(s: String ) : Int =
  val n = s.filter( Character.isDigit )
  if n.length < 1 then
    0
  else
    s"${n.take(1)}${n.takeRight(1)}".toInt

val lut: Map[String, Int] = Map(
  "0" -> 0, "zero" -> 0,
  "1" -> 1, "one" -> 1,
  "2" -> 2, "two" -> 2,
  "3" -> 3, "three" -> 3,
  "4" -> 4, "four" -> 4,
  "5" -> 5, "five" -> 5,
  "6" -> 6, "six" -> 6,
  "7" -> 7, "seven" -> 7,
  "8" -> 8, "eight" -> 8,
  "9" -> 9, "nine" -> 9
)

type Index = Int

case class Result( first: Int, last: Int )

def lookup( s: String ) : Option[Int] =
  lut.foldLeft(
    None: Option[Int]
  )(
    (acc,elem) =>
      if acc.isEmpty && s.startsWith(elem._1) then
        return Some(elem._2)
      else
        acc
  )

@tailrec
def stringToResult(s: String, r: Option[Result] = None ) : Option[Result] =
  if s.isEmpty then
    r
  else
    val valueFound : Option[Int] = lookup( s )
    val newResult : Option[Result] = (r,valueFound) match {
      case (None, None) => None
      case (None, Some(value)) => Some(Result(value, value))
      case (Some(_), None) => r
      case (Some(Result(first, _)), Some(value)) => Some(Result(first, value))
    }
    stringToResult(s.drop(1), newResult)

def decode2( s: String ) : Int =
  stringToResult(s).map( result => result.first*10+result.last).getOrElse(0)

def step1( data: List[String] ) : Int =
  data.foldLeft(0)((acc,line) => acc + decode1(line))

def step2( data: List[String] ) : Int =
  data.foldLeft(0)((acc,line) => acc + decode2(line))
 
