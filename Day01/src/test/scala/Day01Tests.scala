package org.maxpagani

import munit._

class Day01Tests extends munit.FunSuite {
  test("Day01 - Part 1") {
    val test: List[String] = List(
      "1abc2",
      "pqr3stu8vwx",
      "a1b2c3d4e5f",
      "treb7uchet"
    )
    assertEquals(step1(test), 142)
  }

  test("Day01 - Part 2") {
    val test: List[String] = List(
      "two1nine",
      "eightwothree",
      "abcone2threexyz",
      "xtwone3four",
      "4nineeightseven2",
      "zoneight234",
      "7pqrstsixteen"
    )

    assertEquals(step2(test), 281)
  }

}
