package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day15Tests extends munit.FunSuite {
  private val test = List(
    "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7"
  )

  test("Day15 - Part1") {
    assertEquals(step1(test), 1320)
  }

  test("Day15 - Part2") {
    assertEquals(step2(test), 145L)
  }
}
