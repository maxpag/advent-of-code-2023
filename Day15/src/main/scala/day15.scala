package org.maxpagani

import scala.annotation.tailrec
import scala.collection.SortedMap

def hash( data: String ) : Int =
  data.foldLeft( 0 )( (acc, c) => (acc + c.toInt)*17  % 256)


def step1( data: List[String] ) : Int =
  val in = data.head.split( "," )
  in.map( hash ).sum

case class Lens( label: String, value: Int )

sealed trait Operation
case class AddLens( lens: Lens, box: Int ) extends Operation
case class RemoveLens( label: String, box: Int ) extends Operation

def parseOperation( text: String ) : Operation =
  text match {
    case s"$boxLabel=$lens" => AddLens( Lens(boxLabel,lens.toInt), hash(boxLabel) )
    case s"$boxLabel-" => RemoveLens( boxLabel, hash(boxLabel) )
  }

def addOrReplaceLens( list: List[Lens], lens: Lens ) : List[Lens] =
  list match {
    case Nil => List(lens)
    case h :: t if h.label == lens.label => lens :: t
    case h :: t => h :: addOrReplaceLens(t, lens)
  }

def removeLens( label: String, list: List[Lens] ) : List[Lens] =
  list match {
    case Nil => Nil
    case h :: t if h.label == label => t
    case h :: t => h :: removeLens(label,t)
  }

def focusingPower( boxes: Array[List[Lens]] ) : Long =
  boxes.zipWithIndex.map {
    case (lenses, boxNr) =>
      lenses.zipWithIndex.map{
        case (lens, lensNr) =>
          (boxNr+1)*(lensNr+1)*lens.value
      }.sum
  }.sum

def step2( data: List[String] ) : Long =
  val in = data.head.split(",")
  val boxes = new Array[List[Lens]](256).map( _ => List.empty[Lens] )
  in.map(parseOperation).foreach {
    case AddLens(lens, box) =>
      boxes(box) = addOrReplaceLens(boxes(box), lens)
    case RemoveLens(label,box) =>
      boxes(box) = removeLens(label,boxes(box))
  }
  focusingPower( boxes )
  