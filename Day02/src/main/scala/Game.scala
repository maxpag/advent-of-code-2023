package org.maxpagani
import org.maxpagani.HandDraw
import org.maxpagani.StoneSet

case class Game( id: Int, draws: List[HandDraw] ) {
  def stonesRequired: StoneSet = {
    draws.map(_.stones).foldLeft(StoneSet.Empty)(max(_,_))
  }
}