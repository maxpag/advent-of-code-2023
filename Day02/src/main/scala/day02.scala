package org.maxpagani

import org.maxpagani.Bag
import org.maxpagani.StoneSet
import org.maxpagani.Game
import org.maxpagani.decodeData

def step1( data: List[String]) : Int =
  val bag = Bag( StoneSet( 12,13,14 ))
  val games = decodeData( data )
  games.foldLeft( 0 )(
    (a,game) =>
      if game.draws.forall( isDrawPossible(bag,_)) then
        a+game.id
      else
        a
  )

def step2( data: List[String]) : Int =
  val games = decodeData( data )
  games.foldLeft(0)(
    (a, game) =>
      a + game.stonesRequired.power
  )