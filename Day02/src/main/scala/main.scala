package org.maxpagani

import scala.io.Source

@main
def main(): Unit = {
  val filename = "data.in"
  val source = Source.fromFile(filename)
  val lines = source.getLines().toList
  source.close()

  println( step1( lines ))
  println( step2( lines ))
}