package org.maxpagani

import org.maxpagani.StoneSet
import org.maxpagani.HandDraw

def decodeDraws( data: Array[String]) : List[org.maxpagani.HandDraw] =
  data.map( _.split(",") )
    .map( _.foldLeft( StoneSet.Empty )(
      (a: StoneSet,d: String) => {
        val s"$value $color" = d.trim
        val intValue = value.toInt
        color match {
          case "red" => a.setRed(intValue)
          case "green" => a.setGreen(intValue)
          case "blue" => a.setBlue(intValue)
          case _ => a
        }
        
      }
    ))
    .map( HandDraw )
    .toList

  
def decodeData( data: List[String]) : List[Game] =
  data.map(_.drop(5).split(":"))
    .map ( a => (a(0).toInt,a(1).split(";")))
    .map( (id,gameDraws) => Game( id, decodeDraws( gameDraws )))