package org.maxpagani

case class StoneSet( r: Int, g: Int, b: Int )
{
  def setRed( newRed: Int ) : StoneSet = StoneSet( newRed, g, b )
  def setGreen( newGreen: Int ) : StoneSet = StoneSet( r, newGreen, b )
  def setBlue( newBlue: Int ) : StoneSet = StoneSet( r, g, newBlue )
  
  def power = r*g*b
}

def max( s1: StoneSet, s2: StoneSet ) : StoneSet =
  StoneSet( s1.r max s2.r, s1.g max s2.g, s1.b max s2.b )

object StoneSet
{
  val Empty: StoneSet = StoneSet(0,0,0)
}

case class Bag( stones: StoneSet )
case class HandDraw( stones: StoneSet )

def isDrawPossible( bag: Bag, draw: HandDraw ) : Boolean =
  draw.stones.r <= bag.stones.r &&
    draw.stones.g <= bag.stones.g &&
    draw.stones.b <= bag.stones.b