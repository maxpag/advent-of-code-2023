package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day24Tests extends munit.FunSuite {
  private val test = List(
    "19, 13, 30 @ -2,  1, -2",
    "18, 19, 22 @ -1, -1, -2",
    "20, 25, 34 @ -2, -2, -4",
    "12, 31, 28 @ -1, -2, -1",
    "20, 19, 15 @  1, -5, -3"
  )

  test("Day24 - Part1") {
    val trajectories = parse(test)
    val count = counter(trajectories, 7.0, 27.0)
    assertEquals(count, 2L)
  }

  test("Day24 - Part2") {
    assertEquals(step2(test), ???)
  }
}
