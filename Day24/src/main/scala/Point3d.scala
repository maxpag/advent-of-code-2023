package org.maxpagani

case class Point3d[N: Fractional]( x: N, y: N, z: N )
