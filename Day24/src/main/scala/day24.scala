package org.maxpagani

import scala.annotation.tailrec
import scala.math.Fractional.Implicits.infixFractionalOps
import scala.math.Ordering.Implicits.infixOrderingOps
import scala.math.abs


case class Trajectory[N: Numeric]( val p: Point3d[N], val v: Vector3d[N] )

/*
A = p0+v0t
B = p1+v1t

A=B <=> p0+v0 t = p1+v1 t

t = (p1-p0)/(v0-v1)

tx = (px1-px0)/(vx0-vx1)
ty = (py1-py0)/(vy0-vy1)

p0+v0 t0 = p1+v1 t1

p0x + vx0 t0 = p1x + v1x t1
p0y + vy0 t0 = p1y + v1y t1

v0x t0 - v1x t1 = p1x - p0x
v0y t0 - v1y t1 = p1y - p0y

-v1y/v1x v0x t0 + vy1 t1 = -v1y/v1x (p1x -p0x)
v0y t0 - v1y t1 = p1y - p0y

(v0y - v1y/v1x) t0 = p1y - p0y - v1y/v1x (p1x -p0x)

(v0y*v1x - v1y) t0 = (p1y - p0y) v1x - v1y (p1x -p0x)
t0 = ((p1y - p0y) vx1 - (p1x -p0x) v1y) / (v0y*v1x - v1y)

 */

def intersect2d[N: Fractional](tra0: Trajectory[N], tra1: Trajectory[N]): Option[Point3d[N]] =
  val p0 = tra0.p
  val p1 = tra1.p
  val v0 = tra0.v
  val v1 = tra1.v
  val delta = p1 - p0
  val den = v0.y*v1.x - v1.y*v0.x
  if den == 0 then
    None
  else
    val t0 = (delta.y*v1.x -delta.x*v1.y) / den
    if t0 < Fractional[N].zero then
      None
    else
      val t1 = (delta.y*v0.x -delta.x*v0.y) / den
      if t1 < Fractional[N].zero then
        None
      else
        val intersection = p0 + v0 * t0
        Some( intersection )

def intersect2dIn[N: Fractional]( min: N, max: N, tra1: Trajectory[N], tra2: Trajectory[N] ) : Boolean =
  val intersect2dT = intersect2d( tra1, tra2 )
  intersect2dT.exists(p => p.x >= min && p.x <= max && p.y >= min && p.y <= max)

def parse( text: List[String] ) : List[Trajectory[Double]] =
  text.map( line =>
    val s"$px, $py, $pz @ $vx, $vy, $vz" = line
    Trajectory( Point3d(px.toDouble,py.toDouble,pz.toDouble), Vector3d(vx.toDouble,vy.toDouble,vz.toDouble) )
  )

def counter[N: Fractional]( data: List[Trajectory[N]], low: N, high: N ) : Long =
  data match
    case Nil => 0
    case head :: tail =>
      val count = tail.count( t => intersect2dIn( low, high, head, t ))
      count + counter( tail, low, high )

def step1( data: List[String] ) : Long =
  val trajectories = parse( data )
  counter( trajectories, 200000000000000.0, 400000000000000.0 )

def step2( data: List[String] ) : Long = ???
