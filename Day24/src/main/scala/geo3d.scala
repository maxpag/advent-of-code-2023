package org.maxpagani

import scala.annotation.targetName
import scala.math.Fractional.Implicits.infixFractionalOps

extension[N:Fractional]( v: Vector3d[N] )
  def -(b: Vector3d[N]) = Vector3d[N](v.x - b.x, v.y - b.y, v.z - b.z)
  def +(b: Vector3d[N]) = Vector3d[N](v.x + b.x, v.y + b.y, v.z + b.z)
  def *(b: N) = Vector3d[N](v.x * b, v.y * b, v.z * b)
  def /(b: N) = Vector3d[N](v.x / b, v.y / b, v.z / b)
  def /(b: Vector3d[N]) = Vector3d[N](v.x / b.x, v.y / b.y, v.z / b.z)
  def dot(b: Vector3d[N]) = v.x * b.x + v.y * b.y + v.z * b.z
  def cross(b: Vector3d[N]) = Vector3d[N](v.y * b.z - v.z * b.y, v.z * b.x - v.x * b.z, v.x * b.y - v.y * b.x)
  

extension[N:Fractional]( v: Point3d[N] )
  def -(b: Point3d[N]) = Vector3d[N](v.x - b.x, v.y - b.y, v.z - b.z)
  def +(b: Vector3d[N]) = Point3d[N](v.x + b.x, v.y + b.y, v.z + b.z)

@targetName("subPoint3d")
def -[N: Fractional](a: Point3d[N], b: Point3d[N]) = Vector3d[N](a.x - b.x, a.y - b.y, a.z - b.z)
def diff[N: Fractional](a: Point3d[N], b: Point3d[N]) = Vector3d[N](a.x - b.x, a.y - b.y, a.z - b.z)
def *[N: Fractional](a: N, b: Vector3d[N]) = Vector3d[N](a * b.x, a * b.y, a * b.z)
//def *[N: Fractional](a: Vector3d[N], b: N ) = b*a

def diff[N: Fractional](a: Vector3d[N], b: Vector3d[N]) = Vector3d[N](a.x - b.x, a.y - b.y, a.z - b.z)
