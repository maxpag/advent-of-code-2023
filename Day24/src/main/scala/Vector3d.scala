package org.maxpagani

import math.Fractional.Implicits.infixFractionalOps
import math.Integral.Implicits.infixIntegralOps
import scala.annotation.targetName

case class Vector3d[N: Fractional](x: N, y: N, z: N)

def componentWiseDivision[N: Fractional](v: Vector3d[N], d: Vector3d[N]): Vector3d[N] = Vector3d(v.x / d.x, v.y / d.y, v.z / d.z)
