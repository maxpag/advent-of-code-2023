package org.maxpagani

import org.maxpagani._

import scala.collection.SortedSet
import org.maxpagani.TranslationRange

object TranslationMultiRange:
  val empty: TranslationMultiRange = TranslationMultiRange( SortedSet.empty[TranslationRange] )

case class TranslationMultiRange(ranges: SortedSet[TranslationRange] ) {
  def addRange( range: TranslationRange ): TranslationMultiRange = {
    val newRanges = ranges.union( SortedSet(range))
    TranslationMultiRange( newRanges )
  }

  def translate(value: Long ): Option[Long] = {
    ranges.find( _.contains(value)).flatMap( _.translate(value))
  }

  def translate( values: List[Long]) : List[Long] = {
    values.map( x => translate(x).getOrElse(x) )
  }

  def translateRange( range: Range ): List[Range] = {
    case class Acc( in: List[Range], out: List[Range])
    val r = ranges.toList.foldLeft(
      Acc(List(range),List.empty[Range])
    )(
      (acc, translation) =>

        acc.in.map( range => translation.translateRange(range) )
          .foldLeft( Acc( List.empty[Range], acc.out))(
            (acc2, translationResult) =>
              Acc( acc2.in ++ translationResult.notTranslated, acc2.out ++ translationResult.translated )
          )

    )
    r.in ++ r.out
  }
}

