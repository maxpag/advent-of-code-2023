package org.maxpagani

case class TranslationResult( translated: List[Range], notTranslated: List[Range] )

case class TranslationRange(from: Long, to: Long, count: Long ) extends Ordered[TranslationRange] {
  def translate(value: Long ): Option[Long] = {
    if contains( value ) then
      Some(value - from + to)
    else
      None
  }

  def contains( value: Long ): Boolean =
    value >= from && value < from+count

  def isOverlapping( other: TranslationRange ): Boolean =
    from+count > other.from && other.from+other.count > from

  override def compare(that: TranslationRange): Int = {
    if from < that.from then
      -1
    else if from > that.from then
      1
    else
      0
  }

  def isBefore( other: TranslationRange ): Boolean =
    from+count <= other.from

  def isTargetBefore( other: TranslationRange ): Boolean =
    to+count <= other.from

  def translateRange( range: Range ) : TranslationResult =
    if range.last < from || from+count <= range.from then
      TranslationResult(List.empty,List(range))
    else
      if range.from < from then
        val range0size = from - range.from
        val untranslatedBegin = Range(range.from, range0size)
        if range.last < from+count then
          TranslationResult( List(Range(to, range.count-range0size)), List(untranslatedBegin) )
        else
          val lastSize = range.count - range0size-count
          TranslationResult(
            List(Range(to, count)),
            List( untranslatedBegin, Range(from+count, lastSize) )
          )
      else
        val offset = range.from - from
        if range.last < from+count then
          TranslationResult( List(Range(to+offset, range.count)), List.empty )
        else
          val lastSize = range.count-(count - offset)
          TranslationResult(List(Range(to+offset, count-offset)),List(Range(from+count,lastSize)))
}
