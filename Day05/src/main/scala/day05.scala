package org.maxpagani

import scala.annotation.tailrec

@tailrec
def translate( seeds: List[Long], maps: Map[String,Translation], label: String = "seed") : List[Long] =
  if label == "location" then
    seeds
  else
    val translation = maps(label)
    val newSeeds = translation.map.translate( seeds )
    translate( newSeeds, maps, translation.toLabel )

@tailrec
def tabulate( x: Long, count: Long, acc: List[Long] = List.empty ) : List[Long] =
  if count == 0 then
    List.empty
  else
    tabulate( x + 1, count-1, x :: acc )

def computeStep2Seeds(value: List[Long]) : List[Long] =
  value.grouped(2).flatMap( l => {
    val start = l.head
    val count = l.tail.head
    tabulate(start, count)
  }).toList

def computeStep2SeedRanges(value: List[Long]): List[Range] =
  value.grouped(2).toList.map(l => Range(l.head, l.tail.head))
      
  
@tailrec
def translateRange(
  seeds: List[Range],
  maps: Map[String,Translation], label: String = "seed"
) : List[Range] =
  if label == "location" then
    seeds
  else
    val translation = maps(label)
    val newSeeds = seeds.flatMap( translation.map.translateRange )
    translateRange( newSeeds, maps, translation.toLabel )

def step1( data: List[String] ) : Long =
  val result = parse( data )
  val positions = translate( result.seeds, result.maps )
  positions.min

def step2( data: List[String] ) : Long =
  val result = parse(data)
  val seeds = computeStep2SeedRanges(result.seeds)
  val positions = translateRange(seeds, result.maps)
  positions.map( _.from ).min
