package org.maxpagani

private def parseSeeds( line: String ) : List[Long] =
  line.drop("seeds: ".length).split(" ").map(_.toLong).toList


sealed trait ParseState

case object TranslationData extends ParseState
case object EmptyLine extends ParseState

object FoldState:
  val empty: FoldState = FoldState(EmptyLine, Map.empty, "", TranslationMultiRange.empty)

case class FoldState(
  parserState: ParseState,
  map: Map[String,Translation],
  currentLabel: String,
  accumulatedMaps: TranslationMultiRange
)

private def foldEmptyLine( state: FoldState, line: String ) : FoldState = {
  if line.isEmpty then
    state
  else
    state.copy(
      parserState = TranslationData,
      currentLabel = line,
      accumulatedMaps = TranslationMultiRange.empty
    )
}

private def parseRange( line: String ) : TranslationRange =
  val s"$to $from $count" = line
  TranslationRange(from.toLong, to.toLong, count.toLong)

private def foldTranslationData( state: FoldState, line: String ) : FoldState = {
  if line.isEmpty then
    val s"$labelFrom-to-$labelTo map:" = state.currentLabel
    state.copy(
      parserState = EmptyLine,
      currentLabel = "",
      map = state.map + (labelFrom -> Translation(labelTo, state.accumulatedMaps)),
      accumulatedMaps = TranslationMultiRange.empty
    )
  else
    state.copy(accumulatedMaps = state.accumulatedMaps.addRange(parseRange(line)))
}

private def parseMaps( data: List[String] ) : Map[String,Translation] =
  val result = data.foldLeft( FoldState.empty )( (acc, line) => {
    acc.parserState match {
      case EmptyLine =>
        foldEmptyLine(acc, line)
      case TranslationData =>
        foldTranslationData(acc, line)
    }
  })
  foldTranslationData(result,"").map

def parse(data: List[String] ) : SeedData =
  val seedData = parseSeeds(data.head)
  val maps = parseMaps(data.drop(2))
  SeedData(seedData, maps)


