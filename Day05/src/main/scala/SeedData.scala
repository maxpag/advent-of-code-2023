package org.maxpagani

import org.maxpagani.TranslationMultiRange

object Translation:
  val empty: Translation = Translation("",TranslationMultiRange.empty)

case class Translation(toLabel: String, map: TranslationMultiRange )

case class SeedData( seeds: List[Long], maps: Map[String,Translation] )
