package org.maxpagani

case class Range( from: Long, count: Long):
  def last: Long = from + count - 1

  def splitAt( pos: Long): (Range, Range) =
    (Range( from, pos - from+1), Range( pos + 1, last - pos))

  def contains( pos: Long): Boolean = pos >= from && pos <= last

  def contains( range: Range): Boolean = range.from >= from && range.last <= last

  def overlaps( range: Range): Boolean =
    contains( range.from) || contains( range.last) || range.contains( this)