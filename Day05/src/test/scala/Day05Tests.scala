package org.maxpagani

import munit.*
import org.maxpagani.step1
import org.maxpagani.step2

import scala.collection.SortedSet

class Day05Tests extends munit.FunSuite {
  private val test = List(
    "seeds: 79 14 55 13",
    "",
    "seed-to-soil map:",
    "50 98 2",
    "52 50 48",
    "",
    "soil-to-fertilizer map:",
    "0 15 37",
    "37 52 2",
    "39 0 15",
    "",
    "fertilizer-to-water map:",
    "49 53 8",
    "0 11 42",
    "42 0 7",
    "57 7 4",
    "",
    "water-to-light map:",
    "88 18 7",
    "18 25 70",
    "",
    "light-to-temperature map:",
    "45 77 23",
    "81 45 19",
    "68 64 13",
    "",
    "temperature-to-humidity map:",
    "0 69 1",
    "1 0 69",
    "",
    "humidity-to-location map:",
    "60 56 37",
    "56 93 4"
  )

  test("range") {
    val rng0 = Range(1, 9)
    assertEquals(rng0.last, 9L)

    assertEquals(rng0.splitAt(4), (Range(1, 4), Range(5, 5)))
    assertEquals(rng0.splitAt(1), (Range(1, 1), Range(2, 8)))
    assertEquals(rng0.splitAt(8), (Range(1, 8), Range(9, 1)))

    assert( rng0.contains(Range( 4, 2)))
    assert( rng0.contains(Range( 1, 1)))
    assert( rng0.contains(Range( 8, 1)))
    assert( !rng0.contains(Range( 0, 1)))
    assert( !rng0.contains(Range( 10, 1)))
    assert( !rng0.contains(Range( 0, 10)))
    assert( !rng0.contains(Range( 0, 5)))
    assert( !rng0.contains(Range( 5, 7)))

    assert( rng0.overlaps(Range( 4, 2)))
    assert( rng0.overlaps(Range( 1, 1)))
    assert( rng0.overlaps(Range( 8, 1)))
    assert( !rng0.overlaps(Range( 0, 1)))
    assert( !rng0.overlaps(Range( 10, 1)))
    assert( rng0.overlaps(Range( 0, 10)))
    assert( rng0.overlaps(Range( 0, 5)))
    assert( rng0.overlaps(Range( 5, 7)))

  }


  test("translate range") {
    val tr0 = TranslationRange(10, 100, 5)
    val rng0 = Range(1, 9)
    assertEquals( tr0.translateRange(rng0), TranslationResult(List.empty,List(Range(1,9))))
    val rng1 = Range(4, 8)
    assertEquals( tr0.translateRange(rng1), TranslationResult(List(Range(100,2)),List(Range(4,6))))
    val rng2 = Range(4, 20)
    assertEquals( tr0.translateRange(rng2), TranslationResult(List(Range(100,5)),List(Range(4,6),Range(15,9))))
    val rng3 = Range(12, 7)
    assertEquals( tr0.translateRange(rng3), TranslationResult(List(Range(102,3)),List(Range(15,4))))
    val rng4 = Range(10,5)
    assertEquals( tr0.translateRange(rng4), TranslationResult(List(Range(100,5)),List.empty))

    val tr1 = TranslationRange(50, 52, 48)
    val rng5 = Range(79, 14)
    assertEquals( tr1.translateRange(rng5), TranslationResult(List(Range(29+52,14)),List.empty))

  }


  test( "translate multi-range") {
    val dataIn = List(Range(79, 14), Range(55, 13))
    val translation = TranslationMultiRange( SortedSet( TranslationRange(50, 52, 48), TranslationRange(98, 50, 2)))
    val expected = List(Range(29+52,14),Range(57,13))
    assertEquals( translation.translateRange( dataIn.head ), expected.take(1))
    assertEquals( translation.translateRange( dataIn.tail.head ), expected.tail.take(1))
  }

  test( "Range - translation" ) {
    assertEquals( TranslationRange( 98, 50, 2).translate(98),Some(50L))
    assertEquals( TranslationRange( 98, 50, 2).translate(99),Some(51L))
    assertEquals( TranslationRange( 98, 50, 2).translate(100),None)
  }

  test( "Range - translation 2") {
    val rng0 = TranslationRange(45, 81, 19)
    val rng1 = TranslationRange(64, 68, 13)
    val rng2 = TranslationRange(77, 45, 23)
    val input = Range(77,1)
    val expected = List(Range(45,1))

    assertEquals( rng0.translateRange(input), TranslationResult(List.empty,List(input)))
    assertEquals( rng1.translateRange(input), TranslationResult(List.empty,List(input)))
    assertEquals( rng2.translateRange(input), TranslationResult(expected,List.empty))
  }

  test( "Range - part2 partial solution") {
    // In the above example, the lowest location number can be obtained from
    // seed number 82, which corresponds to soil 84, fertilizer 84, water 84,
    // light 77, temperature 45, humidity 46, and location 46. So, the lowest
    // location number is 46.
    val seed = Range(82, 1)
    val seedToSoil = TranslationMultiRange( SortedSet( TranslationRange(50, 52, 48), TranslationRange(98, 50, 2)))
    val soil = List(Range(84,1))
    assertEquals( seedToSoil.translateRange( seed ), soil)

    val soilToFertilized = TranslationMultiRange( SortedSet( TranslationRange(15, 0, 37), TranslationRange(52, 37, 2), TranslationRange(0, 39, 15)))
    val fertilizer = List(Range(84,1))
    assertEquals( soilToFertilized.translateRange( soil.head ), fertilizer)

    val fertilizerToWater = TranslationMultiRange( SortedSet(
      TranslationRange( 53, 49, 8 ),
      TranslationRange( 11, 0, 42 ),
      TranslationRange( 0, 42, 7 ),
      TranslationRange( 7, 57, 4 )))
    val water = List(Range(84,1))
    assertEquals( fertilizerToWater.translateRange( fertilizer.head ), water)

    val waterToLight = TranslationMultiRange( SortedSet(
      TranslationRange( 18, 88, 7 ),
      TranslationRange( 25, 18, 70 )))
    val light = List(Range(77,1))
    assertEquals( waterToLight.translateRange( water.head ), light)

    val lightToTemperature = TranslationMultiRange( SortedSet(
      TranslationRange( 77, 45, 23 ),
      TranslationRange( 45, 81, 19 ),
      TranslationRange( 64, 68, 13 )))
    val temperature = List(Range(45,1))
    assertEquals( lightToTemperature.translateRange( light.head ), temperature)

    val temperatureToHumidity = TranslationMultiRange( SortedSet(
      TranslationRange( 69, 0, 1 ),
      TranslationRange( 0, 1, 69 )))
    val humidity = List(Range(46,1))
    assertEquals( temperatureToHumidity.translateRange( temperature.head ), humidity)
  }

  test("Day05 - Part1") {
    assertEquals(step1(test), 35L)
  }

  test("Day05 - Part2") {
    assertEquals(step2(test), 46L)
  }
}
