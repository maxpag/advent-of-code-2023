package org.maxpagani

import scala.annotation.tailrec

sealed trait Spring( ch: Char ) {
  override def toString: String = ch.toString
}

case object Operational extends Spring('.')
case object Damaged extends Spring('#')
case object Unknown extends Spring('?')

def springsToText( springs: List[Spring] ) : String = springs.map(_.toString).mkString

@tailrec
def doesGroupFit( record: List[Spring], group: Int) : Boolean = {
  group match
    case 0 => record.isEmpty || record.head != Damaged
    case n => if record.isEmpty then false
              else if record.head != Operational then doesGroupFit(record.tail, n - 1)
              else false
}

case class Record(springs: List[Spring], groups: List[Int] )

def textToRecord( text: String ) : Record = {
  val s"$springsText $groups" = text
  val springs: List[Spring] = springsText.map {
    case '.' => Operational
    case '#' => Damaged
    case _ => Unknown
  }.toList
  val groupList = groups.split(',').map(_.toInt).toList
  Record(springs, groupList)
}

def countArrangements( record: List[Spring], groups: List[Int]) : Int = {
  if groups.isEmpty then 1
  else
    var actualRecord = record.dropWhile(_ == Operational )
    var count = 0
    val limit = groups.length - 1 + groups.sum
    while actualRecord.length >= limit do
      if doesGroupFit(actualRecord, groups.head) then
        val springs = actualRecord.drop(groups.head + 1)
        val arrangements = countArrangements(springs, groups.tail)
        count += arrangements
      actualRecord = if actualRecord.head == Damaged then
        List.empty
      else
        actualRecord.drop(1)
    count
}

def step1( data: List[String] ) : Long =
  data.map( line =>
    val record = textToRecord(line)
    countArrangements(record.springs, record.groups)
  )
  .sum


def step2( data: List[String] ) : Long = ???
