package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day12Tests extends munit.FunSuite {
  private val test = List(
    "???.### 1,1,3",
    ".??..??...?##. 1,1,3",
    "?#?#?#?#?#?#?#? 1,3,1,6",
    "????.#...#... 4,1,1",
    "????.######..#####. 1,6,5",
    "?###???????? 3,2,1"
  )

  test("text to record") {
    val line0 = "???.### 1,1,3"
    val record0 = textToRecord(line0)
    assertEquals(record0._1, List(Unknown, Unknown, Unknown, Operational, Damaged, Damaged, Damaged))
    assertEquals(record0._2, List(1, 1, 3))

    val line1 = ".??..??...?##. 1,1,3"
    val record1 = textToRecord(line1)
    assertEquals(record1._1, List( Operational,Unknown,Unknown,Operational,Operational,Unknown,Unknown,Operational,Operational,Operational,Unknown, Damaged, Damaged, Operational ))
    assertEquals(record1._2, List(1, 1, 3))
  }
  
  test("Count line 1") {
    val line = "???.### 1,1,3"
    val record = textToRecord(line)
    assertEquals(countArrangements(record.springs, record.groups), 1)
  }

  test("Count line 2") {
    val line = ".??..??...?##. 1,1,3"
    val record = textToRecord(line)
    assertEquals(countArrangements(record.springs, record.groups), 4)
  }

  test("Count line 10") {
    val line = "?###???????? 3,2,1"
    val record = textToRecord(line)
    val count = countArrangements(record.springs, record.groups)
    assertEquals(count, 10)
  }

  test("Count line data 1") {
    val line1 = "??#??????#..????? 9,2,1"
    val record1 = textToRecord(line1)
    assertEquals(countArrangements(record1.springs, record1.groups), 3)

    val line2 = "???##??#?.?#? 5,1,2"
    val record2 = textToRecord(line2)
    assertEquals(countArrangements(record2.springs, record2.groups), 4)

    val line3 = "????.????. 1,1"
    val record3 = textToRecord(line3)
    assertEquals(countArrangements(record3.springs, record3.groups), 22)

    val line4 = ".?#?????###???. 1,6,1"
    val record4 = textToRecord(line4)
    assertEquals(countArrangements(record4.springs, record4.groups), 3)

    val line5 = "????#?.??? 2,1,1"
    val record5 = textToRecord(line5)
    assertEquals(countArrangements(record5.springs, record5.groups), 8)
  }

  test("doesGroupFit true - 1") {
    val line = "???.### 3"
    val record = textToRecord(line)
    assertEquals(doesGroupFit(record.springs, record.groups.head), true)
  }

  test("doesGroupFit true - 2") {
    val line = "?#?.### 3"
    val record = textToRecord(line)
    assertEquals(doesGroupFit(record.springs, record.groups.head), true)
  }

  test("doesGroupFit false - 1") {
    val line = "???#.### 3"
    val record = textToRecord(line)
    assertEquals(doesGroupFit(record.springs, record.groups.head), false)
  }

  test("doesGroupFit true - 2") {
    val line = "####.### 3"
    val record = textToRecord(line)
    assertEquals(doesGroupFit(record.springs, record.groups.head), false)
  }

  test("Day12 - Part1") {
    assertEquals(step1(test), 21L)
  }

  test("Day12 - Part2") {
    assertEquals(step2(test), 0L)
  }
}
