package org.maxpagani

case class SymbolParser( mid: String, bottom: String, result: List[String] )

def isSymbol(c: Char ) : Boolean = !(c.isDigit || c == '.')

def mergeSymbolRows(top: String, mid: String, bottom: String ): String =
  top.lazyZip(mid).lazyZip(bottom)
    .map( (a,b,c) =>
      if isSymbol(a) || isSymbol(b) || isSymbol(c) then '$' else '.'
    ).mkString

def collapseSymbols( top: String, mid: String, bottom: String ): String =
  val s = mergeSymbolRows( top, mid, bottom )
  val left = s.drop(1)+'.'
  val right = '.'+s.dropRight(1)
  mergeSymbolRows( left, s, right )

def extractSymbols(data: List[String]): List[ String ] =
  val top : String = data.slice(0, 1).head
  val mid : String = data.slice(1, 2).head
  val bottom : String = data.slice(2, 3).head
  val initial = collapseSymbols(top,mid,bottom)
  data.drop(3).foldLeft( SymbolParser( mid, bottom, List(initial) )  )(
    (a,line) => SymbolParser( a.bottom, line, a.result :+ collapseSymbols(a.mid,a.bottom,line))
  ).result

sealed trait ScanState
case object Scan extends ScanState
case object IntegerFound extends ScanState
case object PartNumberFound extends ScanState

case class FoldState( number: Int, sum: Int, state: ScanState )

def sumLineParts( line: String, symbols: String ) : Int =
  val result = line
    .lazyZip(symbols)
    .foldLeft( FoldState(0,0,Scan) )( (a,pair) =>
      a.state match {
        case Scan =>
          if pair._1.isDigit then
            FoldState(
              pair._1.asDigit,
              a.sum,
              if pair._2 == '$' then PartNumberFound else IntegerFound
            )
          else a
        case IntegerFound =>
          if pair._1.isDigit then
            FoldState(
              a.number*10 + pair._1.asDigit,
              a.sum,
              if pair._2 == '$' then PartNumberFound else IntegerFound
            )
          else
            FoldState(0,a.sum,Scan)
        case PartNumberFound =>
          if pair._1.isDigit then
            FoldState(
              a.number*10 + pair._1.asDigit,
              a.sum,
              PartNumberFound
            )
          else
            FoldState(0,a.sum+a.number,Scan)
      }
    )
  if result.state == PartNumberFound then result.sum + result.number else result.sum



def step1( data: List[String]) : Int =
  val l : Int = data(0).length
  val padding = "." * l
  val paddedData = padding :: data ::: List(padding)

  val symbols = extractSymbols( paddedData )
  data
    .lazyZip(symbols)
    .foldLeft(0)( (a,s) => a + sumLineParts(s._1,s._2) )

def step2(data: List[String]): Int =
  ???