
import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day02Tests extends munit.FunSuite {
  private val test = List(
    "467..114..",
    "...*......",
    "..35..633.",
    "......#...",
    "617*......",
    ".....+.58.",
    "..592.....",
    "......755.",
    "...$.*....",
    ".664.598.."
  )

  test("Day03 - Part1") {
    assertEquals(step1(test), 4361)
  }
/*
  test("Day03 - Part2") {
    assertEquals(step2(test), 2286)
  }
*/
}
