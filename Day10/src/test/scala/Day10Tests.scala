package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day10Tests extends munit.FunSuite {
  private val test1 = List(
    "-L|F7",
    "7S-7|",
    "L|7||",
    "-L-J|",
    "L|-JF"
  )
  
  private val test2 = List(
    "..F7.",
    ".FJ|.",
    "SJ.L7",
    "|F--J",
    "LJ..."
  )

  private val test3 = List(
  "..........",
  ".S------7.",
  ".|F----7|.",
  ".||....||.",
  ".||....||.",
  ".|L-7F-J|.",
  ".|..||..|.",
  ".L--JL--J.",
  ".........."
  )

  private val test4 = List(
    ".F----7F7F7F7F-7....",
    ".|F--7||||||||FJ....",
    ".||.FJ||||||||L7....",
    "FJL7L7LJLJ||LJ.L-7..",
    "L--J.L7...LJS7F-7L7.",
    "....F-J..F7FJ|L7L7L7",
    "....L7.F7||L7|.L7L7|",
    ".....|FJLJ|FJ|F7|.LJ",
    "....FJL-7.||.||||...",
    "....L---J.LJ.LJLJ..."
  )

  test( "Day10 - count inside" ) {
    val line = parseLine(".|L-7F-J|.")
    assertEquals( countLineInside( line ), 0L )
  }

  test("Day10 - Part1 - simple") {
    assertEquals(step1(test1), 4L)
  }

  test("Day10 - Part1 - complex" ) {
    assertEquals(step1(test2), 8L)
  }
  
  test("Day10 - Part2 - simple") {
    assertEquals(step2(test3), 4L)
  }

  test("Day10 - Part2 - complex") {
    assertEquals(step2(test4), 8L)
  }
}
