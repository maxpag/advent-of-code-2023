package org.maxpagani

import scala.annotation.tailrec

enum Pipe:
  case Vertical
  case Horizontal
  case NorthToEast
  case NorthToWest
  case SouthToEast
  case SouthToWest
  case Ground
  case StartPosition

  override def toString: String = this match {
    case Vertical => "|"
    case Horizontal => "-"
    case NorthToEast => "L"
    case NorthToWest => "J"
    case SouthToEast => "F"
    case SouthToWest => "7"
    case Ground => "."
    case StartPosition => "S"
  }

case class Position( x: Int, y: Int )

enum Facing:
  case North
  case South
  case East
  case West

def hasConnection( p: Pipe, f: Facing ) : Boolean =
  (p, f) match {
    case (Pipe.Vertical, Facing.North) => true
    case (Pipe.Vertical, Facing.South) => true
    case (Pipe.Horizontal, Facing.East) => true
    case (Pipe.Horizontal, Facing.West) => true
    case (Pipe.NorthToEast, Facing.North) => true
    case (Pipe.NorthToEast, Facing.East) => true
    case (Pipe.NorthToWest, Facing.North) => true
    case (Pipe.NorthToWest, Facing.West) => true
    case (Pipe.SouthToEast, Facing.South) => true
    case (Pipe.SouthToEast, Facing.East) => true
    case (Pipe.SouthToWest, Facing.South) => true
    case (Pipe.SouthToWest, Facing.West) => true
    case _ => false
  }

def nextFacing( from: Facing, pipe: Pipe ) : Facing =
  (from, pipe) match {
    case (Facing.North, Pipe.Vertical) => Facing.North
    case (Facing.North, Pipe.SouthToEast) => Facing.East
    case (Facing.North, Pipe.SouthToWest) => Facing.West

    case (Facing.South, Pipe.Vertical) => Facing.South
    case (Facing.South, Pipe.NorthToEast) => Facing.East
    case (Facing.South, Pipe.NorthToWest) => Facing.West

    case (Facing.East, Pipe.Horizontal) => Facing.East
    case (Facing.East, Pipe.NorthToWest) => Facing.North
    case (Facing.East, Pipe.SouthToWest) => Facing.South

    case (Facing.West, Pipe.Horizontal) => Facing.West
    case (Facing.West, Pipe.NorthToEast) => Facing.North
    case (Facing.West, Pipe.SouthToEast) => Facing.South
  }

def next( from: Position, facing: Facing ) : Position =
  facing match {
    case Facing.North => Position(from.x, from.y - 1)
    case Facing.South => Position(from.x, from.y + 1)
    case Facing.East => Position(from.x + 1, from.y)
    case Facing.West => Position(from.x - 1, from.y)
  }

def parseLine( line: String ) : Array[Pipe] =
  line.map {
    case '|' => Pipe.Vertical
    case '-' => Pipe.Horizontal
    case 'L' => Pipe.NorthToEast
    case 'J' => Pipe.NorthToWest
    case 'F' => Pipe.SouthToEast
    case '7' => Pipe.SouthToWest
    case '.' => Pipe.Ground
    case 'S' => Pipe.StartPosition
    case _ => throw new Exception("Invalid character")
  }.toArray

def locateStart( pipes: Array[Array[Pipe]] ) : Position =
  val y = pipes.indexWhere(_.contains(Pipe.StartPosition))
  val x = pipes(y).indexOf(Pipe.StartPosition)
  Position(x, y)

def availableFacings( map: Array[Array[Pipe]], position: Position ) : List[Facing] =
  val width = map(0).size
  val height = map.size
  val northPipe = if position.y > 0 then Some(map(position.y - 1)(position.x)) else None
  val southPipe = if position.y < height-1 then Some(map(position.y + 1)(position.x)) else None
  val eastPipe = if position.x < width-1 then Some(map(position.y)(position.x + 1)) else None
  val westPipe = if position.x > 0 then  Some(map(position.y)(position.x - 1)) else None
  List(
    northPipe.flatMap( p => if hasConnection( p, Facing.South ) then Some(Facing.North) else None),
    southPipe.flatMap( p => if hasConnection( p, Facing.North ) then Some(Facing.South) else None),
    eastPipe.flatMap( p => if hasConnection( p, Facing.West ) then Some(Facing.East) else None),
    westPipe.flatMap( p => if hasConnection( p, Facing.East ) then Some(Facing.West) else None)
  ).flatten


def availableFacingToPipe( facings: List[Facing] ) : Pipe =
  val first = facings.head
  val second = facings(1)
  val pair = if first == Facing.North || first == Facing.South
    then (first, second)
    else (second, first)
  pair match {
    case (Facing.North,Facing.East) => Pipe.NorthToEast
    case (Facing.North,Facing.West) => Pipe.NorthToWest
    case (Facing.South,Facing.East) => Pipe.SouthToEast
    case (Facing.South,Facing.West) => Pipe.SouthToWest
  }

def parse( data: List[String] ) : Array[Array[Pipe]] =
  data.map(parseLine).toArray

case class Walker( facing: Facing, position: Position, steps: Int )

def step1( data: List[String] ) : Long =
  val map = parse(data)
  val position = locateStart(map)
  val facings = availableFacings( map, position )
  assert( facings.size == 2 )

  val walkers = facings.map( f => Walker(f, position, 1) )

  @tailrec
  def run( walkers: List[Walker], stepCount: Int ) : Int =
    if stepCount != 0 && walkers.drop(1).forall( _.position == walkers.head.position )
    then stepCount
    else
      val nextWalkers = walkers.map( w => {
        val updatedPosition = next( w.position, w.facing )
        val updateFacing = nextFacing( w.facing, map(updatedPosition.y)(updatedPosition.x) )
        Walker(updateFacing, updatedPosition, w.steps + 1)
      })
      run( nextWalkers, stepCount + 1 )

  run( walkers, 0 )



@tailrec
def run[S](
  walker: Walker,
  state: S,
  update: (S,Walker)=>S,
  isComplete: (S,Walker) => Boolean,
  map: Array[Array[Pipe]]
) : S =
  if isComplete( state, walker )
  then state
  else
    val updatedPosition = next( walker.position, walker.facing )
    val updateFacing = nextFacing( walker.facing, map(updatedPosition.y)(updatedPosition.x) )
    val nextWalker = Walker(updateFacing, updatedPosition, walker.steps + 1)
    run( nextWalker, update(state,nextWalker), update, isComplete, map )

def countLineInside( line: Array[Pipe] ) : Long =
  enum State:
    case Outside
    case Inside
    case OutsideSouthSkip
    case OutsideNorthSkip
    case InsideSouthSkip
    case InsideNorthSkip

  case class FoldState( state: State, count: Long )
  line.foldLeft( FoldState(State.Outside,0L) )(
    (s,p) =>
      (s.state, p) match {
        case (State.Outside, Pipe.Ground) => s
        case (State.Outside, Pipe.Vertical) => s.copy(state = State.Inside)
        case (State.Outside, Pipe.NorthToEast) => s.copy(state = State.OutsideNorthSkip)
        case (State.Outside, Pipe.SouthToEast) => s.copy(state = State.OutsideSouthSkip)
        case (State.Inside, Pipe.Ground) => s.copy(count = s.count + 1)
        case (State.Inside, Pipe.Vertical) => s.copy(state = State.Outside)
        case (State.Inside, Pipe.NorthToEast) => s.copy(state = State.InsideNorthSkip)
        case (State.Inside, Pipe.SouthToEast) => s.copy(state = State.InsideSouthSkip)

        case (_, Pipe.Horizontal) => s
        case (State.OutsideNorthSkip, Pipe.NorthToWest) => s.copy(state = State.Outside)
        case (State.OutsideNorthSkip, Pipe.SouthToWest) => s.copy(state = State.Inside)
        case (State.OutsideSouthSkip, Pipe.NorthToWest) => s.copy(state = State.Inside)
        case (State.OutsideSouthSkip, Pipe.SouthToWest) => s.copy(state = State.Outside)

        case (State.InsideNorthSkip, Pipe.NorthToWest) => s.copy(state = State.Inside)
        case (State.InsideNorthSkip, Pipe.SouthToWest) => s.copy(state = State.Outside)
        case (State.InsideSouthSkip, Pipe.NorthToWest) => s.copy(state = State.Outside)
        case (State.InsideSouthSkip, Pipe.SouthToWest) => s.copy(state = State.Inside)
      }
  ).count

def countInside(path: Array[Array[Pipe]]) : Long =
  path.map( countLineInside ).sum

def step2( data: List[String] ) : Long =
  val map = parse(data)
  val position = locateStart(map)
  val facings = availableFacings(map, position)
  val walker = Walker( facings.head, position, 0 )

  val terseMap = new Array[Array[Pipe]](map.length)
  for y <- map.indices do
    terseMap(y) = new Array[Pipe](map(y).length)
    for x <- map(y).indices do
      if x == position.x && y == position.y
      then
        val pipe = availableFacingToPipe(facings)
        terseMap(y)(x) = pipe
        map(y)(x) = pipe
      else
        terseMap(y)(x) = Pipe.Ground
    end for
  end for

  case class State( map: Array[Array[Pipe]], count: Int )
  val state = State(terseMap,0)
  val path = run(
    walker,
    state, (s,w) => {
      if s.map(w.position.y)(w.position.x) == Pipe.Ground
      then s.map(w.position.y)(w.position.x) = map(w.position.y)(w.position.x)
      s.map(w.position.y)(w.position.x) = map(w.position.y)(w.position.x)
      s.copy(count = s.count + 1)
    },
    (s,w) => s.count != 0 && w.position == position,
    map
  ).map

  countInside( path )