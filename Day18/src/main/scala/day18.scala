package org.maxpagani

import scala.annotation.{tailrec, targetName}

enum Facing:
  case Up, Down, Left, Right

case class Color( hex: String )

case class Directive( facing: Facing, count: Int, color: Color )
case class Position( x: Int, y: Int )

def parseLIne( line: String ) : Directive =
  val facing = line(0) match
    case 'U' => Facing.Up
    case 'D' => Facing.Down
    case 'L' => Facing.Left
    case 'R' => Facing.Right

  line.drop(2) match
    case s"$count (#${hexColor})" => Directive( facing, count.toInt, Color(hexColor) )

def fillMap( map: List[String], position: Position, ch: Char ) : List[String] =
  val growMap = if map.length <= position.y then
    map ++ List.fill( position.y - map.length+1 )( "" )
  else
    map
  val (front,back) = growMap.splitAt( position.y )
  val line = back.head
  val newLine = if line.length <= position.x then
    val pad = position.x - line.length
    line+ " "*pad + ch
  else
    line.updated( position.x, ch )
  front ++ List( newLine ) ++ back.tail

def expand( position: Position, directive: Directive ) : List[Position] =
  directive.facing match
    case Facing.Up => (directive.count to 1).map(
        i => Position( position.x, position.y - i )
      ).toList
    case Facing.Down => (1 to directive.count).map(
        i => Position( position.x, position.y + i )
      ).toList
    case Facing.Left => (directive.count to 1).map(
        i => Position( position.x - i, position.y )
      ).toList
    case Facing.Right => (1 to directive.count).map(
        i => Position( position.x + i, position.y )
      ).toList

def expandToFill(position: Position, directive: Directive): List[(Position,Char)] =
  directive.facing match
    case Facing.Up => (1 to directive.count).map(
        i => (Position(position.x, position.y - i),'#')
      ).toList
    case Facing.Down => (1 to directive.count).map(
        i => (Position(position.x, position.y + i),'#')
      ).toList
    case Facing.Left =>
      (1 until directive.count).map(
        i => (Position(position.x - i, position.y),' ')
      ).toList ++ List((Position(position.x-directive.count,position.y),'#'))
    case Facing.Right =>
      (1 until directive.count).map(
        i => (Position(position.x + i, position.y),' ')
      ).toList ++ List((Position(position.x+directive.count,position.y),'#'))

def drawBorder( directives: List[Directive] ) : List[String] = {
  val grid: List[String] = List("#")
  val position = Position(0, 0)

  case class FoldState(position: Position, grid: List[String])

  directives.foldLeft(
    FoldState(position, grid)
  )((state: FoldState, directive: Directive) => {
    val line = expand(state.position, directive)
    println( s"$directive + $position -> $line" )
    val newMap = line.foldLeft(state.grid)((map, position) =>
      fillMap(map, position, '#')
    )
    println(line.last)
    FoldState(line.last, newMap)
  }).grid
}

def drawBorderToFill(directives: List[Directive]): List[String] = {
  val grid: List[String] = List("#")
  val position = Position(0, 0)

  case class FoldState(position: Position, grid: List[String])

  directives.foldLeft(
    FoldState(position, grid)
  )((state: FoldState, directive: Directive) => {
    val line = expandToFill(state.position, directive)
    val newMap = line.foldLeft(state.grid)((map, position) =>
      println( position._1 )
      fillMap(map, position._1, position._2)
    )
    FoldState(line.last._1, newMap)
  }).grid
}


@targetName("s_+:")
object s_+: {
  def unapply(s: String): Option[(Char, String)] = s.headOption.map {
    (_, s.tail)
  }
}

def fillLine( line: String, fill: Boolean = false ) : String =
  line match
    case "" => ""
    case " " if fill => "#"
    case " " => " "
    case "#" => "#"
    case ' ' s_+: tail if fill =>  s"#${fillLine(tail, fill)}"
    case ' ' s_+: tail if !fill => s" ${fillLine(tail, fill)}"
    case '#' s_+: tail if fill =>
      val ending = line.dropWhile( _ == '#' )
      val hashCount = line.length - ending.length
      if hashCount == 1 then
        s"#${fillLine(ending, false)}"
      else
        s"${'#'*hashCount}${fillLine(ending, true)}"
    case '#' s_+: tail if !fill =>
      val ending = line.dropWhile(_ == '#')
      val hashCount = line.length - ending.length
      if hashCount == 1 then
        s"#${fillLine(ending, true)}"
      else
        s"${"#" * hashCount}${fillLine(ending, false)}"


def fillInnards( grid: List[String] ) : List[String] =
  grid.map( fillLine( _ ) )

def step1( data: List[String] ) : Long =
  val directives = data.map( parseLIne )
  val border = drawBorder( directives )
  //val innards = fillInnards( border )
  //innards.map( _.count( _ == '#' ) ).sum
  border.foreach( println )
  0L

def step2( data: List[String] ) : Long = ???
