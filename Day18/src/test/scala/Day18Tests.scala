package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day18Tests extends munit.FunSuite {
  private val test = List(
    "R 6 (#70c710)",
    "D 5 (#0dc571)",
    "L 2 (#5713f0)",
    "D 2 (#d2c081)",
    "R 2 (#59c680)",
    "D 2 (#411b91)",
    "L 5 (#8ceee2)",
    "U 2 (#caa173)",
    "L 1 (#1b58a2)",
    "U 2 (#caa171)",
    "R 2 (#7807d2)",
    "U 3 (#a77fa3)",
    "L 2 (#015232)",
    "U 2 (#7a21e3)"
  )

  test("Day18 - draw border") {
    val expected: List[String] = List(
      "#######",
      "#     #",
      "###   #",
      "  #   #",
      "  #   #",
      "### ###",
      "#   #",
      "##  ###",
      " #    #",
      " ######"
    )

    val result = drawBorder(test.map( parseLIne ))
    assertEquals(result, expected)
  }

  test("Day18 - draw to fill") {
    val expected: List[String] = List(
      "#     #",
      "#     #",
      "#     #",
      "  #   #",
      "  #   #",
      "#     #",
      "#   #",
      "#     #",
      " #    #",
      " #    #"
    )

    val result = drawBorderToFill( test.map( parseLIne ))
    assertEquals(result, expected)
  }


  test("Day18 - fill border") {
    val expected: List[String] = List(
      "#######",
      "#######",
      "#######",
      "  #####",
      "  #####",
      "#######",
      "#####",
      "#######",
      " ######",
      " ######"
    )

    val border = drawBorder(test.map(parseLIne))
    val filled = fillInnards(border)
    assertEquals(filled, expected)
  }

  test("Day18 - Part1") {
    assertEquals(step1(test), 62L)
  }

  test("Day18 - Part2") {
    assertEquals(step2(test), ???)
  }
}
