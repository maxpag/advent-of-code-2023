package org.maxpagani

import scala.collection.mutable

def groupInput( data: List[String] ) : List[List[String]] =
  data.foldLeft( List[List[String]]() ) { (acc, line) =>
    if (line.isEmpty) List[String]() :: acc
    else if (acc.isEmpty) List(List(line))
    else (line :: acc.head) :: acc.tail
  }.reverse.map( _.reverse )

case class Area( weView: List[Int], nsView: List[Int] )

def listToWeView( data: List[String] ) : List[Int] =
  data.map( line => {
    line.foldLeft(0)( (acc, c) => if (c == '#') acc * 2 + 1 else acc * 2 )
  })

def listToNsView( data: List[String] ) : List[Int] =
  val initial = data.head.map( _ => 0 ).toList
  data.foldLeft( initial )( (acc, line) => {
    acc.zip( line.map( c => if (c == '#') 1 else 0 ) )
      .map( (a, b) => 2*a + b )
  })

def groupToArea( group: List[String] ) : Area =
  Area( listToWeView( group ), listToNsView( group ) )

def parseInput( groups: List[List[String]] ) : List[Area] =
  groups.map( groupToArea )

def findSymmetryCandidates( view: List[Int] ) : List[Int] = {
  view
    .zip( view.tail )
    .zipWithIndex
    .map ( a => (a._1._1, a._1._2, a._2+1) )
    .filter( (a, b, c) => a == b )
    .map(_._3)
}

def isValidSymmetry(value: List[Int], col: Int ) : Boolean =
  val (a0, b0) = value.splitAt( col )
  val (a1, b1) = if a0.length < b0.length then
    (a0, b0.take( a0.length ))
  else
    (a0.drop( a0.length - b0.length ), b0)

  a1.zip( b1.reverse ).forall( (x, y) => x == y )


def findSymmetry( view: List[Int] ) : Int =
  val candidates = findSymmetryCandidates( view )
  if candidates.isEmpty then
    0
  else
    val simmetries = candidates.filter( isValidSymmetry( view, _ ) )
    assert( simmetries.length <= 1 )
    simmetries.headOption.getOrElse( 0 )

def step1( data: List[String] ) : Int =
  val areas = parseInput( groupInput( data ) )
  val symmetries = areas.map( a => {
    100*findSymmetry( a.weView ) + findSymmetry( a.nsView )
  } )
  symmetries.sum

def step2( data: List[String] ) : Int = ???
