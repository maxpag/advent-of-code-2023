package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day13Tests extends munit.FunSuite {
  private val test = List(
  "#.##..##.",
  "..#.##.#.",
  "##......#",
  "##......#",
  "..#.##.#.",
  "..##..##.",
  "#.#.##.#.",
  "",
  "#...##..#",
  "#....#..#",
  "..##..###",
  "#####.##.",
  "#####.##.",
  "..##..###",
  "#....#..#"
  )

  test("Group Input") {
    val groups = groupInput(test)
    val group1 = List(
      "#.##..##.",
      "..#.##.#.",
      "##......#",
      "##......#",
      "..#.##.#.",
      "..##..##.",
      "#.#.##.#."
    )
    val group2 = List(
      "#...##..#",
      "#....#..#",
      "..##..###",
      "#####.##.",
      "#####.##.",
      "..##..###",
      "#....#..#"
    )
    assertEquals(groups, List(group1, group2))
  }

  test("Group we conversion") {
    val group = List(
      "#.##..##.",    // 101100110 -> 0x166
      "..#.##.#.",    // 001011010 -> 0x05a
      "##......#",    // 110000001 -> 0x181
      "##......#",    // 110000001 -> 0x181
      "..#.##.#.",    // 001011010 -> 0x05a
      "..##..##.",    // 001100110 -> 0x066
      "#.#.##.#."     // 101011010 -> 0x15a
    )
    assertEquals(listToWeView(group), List(
      0x166,
      0x05a,
      0x181,
      0x181,
      0x05a,
      0x066,
      0x15a,
    ))
  }

  test("Group ns conversion") {
    val group = List(
      "#.##..##.",
      "..#.##.#.",
      "##......#",
      "##......#",
      "..#.##.#.",
      "..##..##.",
      "#.#.##.#."
   //  ^------------ 1011001 -> 0x59
   //   ^----------- 0011000 -> 0x18
   //    ^---------- 1100111 -> 0x67
   //     ^--------- 1000010 -> 0x42
   //      ^-------- 0100101 -> 0x25
   //       ^------- 0100101 -> 0x25
   //        ^------ 1000010 -> 0x42
   //         ^----- 1100111 -> 0x67
   //          ^---- 0011000 -> 0x18
    )
    assertEquals(listToNsView(group), List(
      0x59,
      0x18,
      0x67,
      0x42,
      0x25,
      0x25,
      0x42,
      0x67,
      0x18
    ))
  }

  test("Symmetry") {
    val list1 = List(
      "#...##..#",
      "#....#..#",
      "..##..###",
      "#####.##.",
      "#####.##.",
      "..##..###",
      "#....#..#"
    )
    val group1a = listToWeView(list1)
    val group1b = listToNsView(list1)
    assertEquals(findSymmetry(group1a), 4)
    assertEquals(findSymmetry(group1b), 0)

    val list2 = List(
      "#.##..##.",
      "..#.##.#.",
      "##......#",
      "##......#",
      "..#.##.#.",
      "..##..##.",
      "#.#.##.#."
    )
    val group2a = listToNsView(list2)
    val group2b = listToWeView(list2)
    assertEquals(findSymmetry(group2a), 5)
    assertEquals(findSymmetry(group2b), 0)
  }


  test("Day13 - Part1") {
    assertEquals(step1(test), 405)
  }

  test("Day13 - Part2") {
    assertEquals(step2(test), ???)
  }
}
