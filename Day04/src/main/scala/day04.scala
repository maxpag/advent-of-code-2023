package org.maxpagani

def parseHand( hand: String ) : Set[Int] =
  hand.split(' ').filter(_.nonEmpty).map( _.trim.toInt ).toSet

def parse(line: String) : (Set[Int],Set[Int]) = {
  val content = line.dropWhile( _ != ':' ).drop(1).trim
  val hands = content.split('|')
  (parseHand(hands(0)),parseHand(hands(1)))
}

def countPoints( card: (Set[Int],Set[Int]) ) : Int = {
  val (winners,hand) = card
  val thisWinners = winners.intersect(hand)
  thisWinners.size
}

def step1Count( card: (Set[Int],Set[Int]) ) : Int = {
  val points = countPoints(card)
  if points == 0 then 0 else 1 << (points-1)
}


def step1( data: List[String] ) : Int =
  data.foldLeft( 0 )( (acc, line) => acc + step1Count(parse(line)) )

case class Step2Counter( count: Int, multipliers: List[Int] )
{
  def next : Step2Counter = {
    val newMultipliers = if multipliers.nonEmpty then multipliers.tail else List.empty
    Step2Counter( count, newMultipliers )
  }
  def addCards( n: Int, copies: Int ) : Step2Counter = {
    val newCount = count+n*copies
    val firstN = multipliers.take(n)
    val restN = multipliers.drop(n)
    val newMultipliers = firstN.map( _+copies ) ++ restN
    Step2Counter( newCount, newMultipliers )
  }
}


def step2( data: List[String] ) : Int =
  val initialCardCount = data.size
  val multipliers = List.fill( initialCardCount )(1)
  data.foldLeft( Step2Counter(initialCardCount, multipliers) )(
    (acc, line) => {
      val currentMultiplier = acc.multipliers.head
      acc.next.addCards(countPoints(parse(line)), currentMultiplier)
    }
  ).count