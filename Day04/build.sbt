ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.1.3"

lazy val root = (project in file("."))
  .settings(
    name := "Day04",
    idePackagePrefix := Some("org.maxpagani"),
    libraryDependencies += "org.scalameta" %% "munit" % "1.0.0-M7" % Test
  )
