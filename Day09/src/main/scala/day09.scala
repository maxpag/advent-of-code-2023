package org.maxpagani

def reduce( data: List[Long] ) : List[Long] =
  data.zip ( data.tail ).map( t => t._2 - t._1 )

def buildReductions( data: List[Long] ) : List[List[Long]] =
  if data.forall( _ == 0 ) then List.empty
  else
    val reduced = reduce(data)
    reduced :: buildReductions( reduced )

def nextValue( data: List[Long] ) : Long =
  val reductions = data :: buildReductions(data)
  reductions.foldLeft( 0L )( (acc,list) => acc + list.last )

def prevValue(data: List[Long]): Long =
  val reductions = data :: buildReductions(data)
  reductions.reverse.foldLeft(0L)((acc, list) => {list.head - acc})


def parse( data: List[String] ) : List[List[Long]] =
  data.map( _.split( " " ).map( _.toLong ).toList )

def step1( data: List[String] ) : Long =
  parse( data ).map( nextValue ).sum

def step2( data: List[String] ) : Long =
  parse(data).map(prevValue).sum
