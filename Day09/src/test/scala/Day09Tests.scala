package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2

class Day09Tests extends munit.FunSuite {
  private val test = List(
    "0 3 6 9 12 15",
    "1 3 6 10 15 21",
    "10 13 16 21 30 45"
  )

  test( "Day09 - nextValue" ) {
    assertEquals(nextValue(List(0L, 3L, 6L, 9L, 12L, 15L)), 18L)
    assertEquals(nextValue(List(1L, 3L, 6L, 10L, 15L, 21L)), 28L)
    assertEquals(nextValue(List(10L, 13L, 16L, 21L, 30L, 45L)), 68L)
  }

  test("Day09 - reduction") {
    assertEquals(reduce(List(0L, 3L, 6L, 9L, 12L, 15L)), List(3L, 3L, 3L, 3L, 3L))
    assertEquals(reduce(List(1L, 3L, 6L, 10L, 15L, 21L)), List(2L, 3L, 4L, 5L, 6L))
    assertEquals(reduce(List(10L, 13L, 16L, 21L, 30L, 45L)), List(3L, 3L, 5L, 9L, 15L))
  }

  test( "Day09 - build reductions") {
    assertEquals(buildReductions(List(0L, 3L, 6L, 9L, 12L, 15L)), List(List(3L, 3L, 3L, 3L, 3L), List(0L,0L,0L,0L)))
    assertEquals(buildReductions(List(1L, 3L, 6L, 10L, 15L, 21L)), List(List(2L, 3L, 4L, 5L, 6L), List(1L,1L,1L,1L), List(0L,0L,0L) ))
    assertEquals(buildReductions(List(10L, 13L, 16L, 21L, 30L, 45L)), List(List(3L, 3L, 5L, 9L, 15L), List(0L,2L,4L,6L), List(2L,2L,2L),List(0L,0L)))

    assertEquals(buildReductions(List( -3L,	7L,	35L,	102L,	237L,	469L,	819L,	1311L,	2036L,	3318L,	6046L,	12251L,	26022L,	54870L,	111664L,	217278L,	404103L,	720593L,	1237029L,	2052700L,	3304715L )),
      List(
List( 10L,	28L,	67L,	135L,	232L,	350L,	492L,	725L,	1282L,	2728L,	6205L,	13771L,	28848L,	56794L,	105614L,	186825L,	316490L,	516436L,	815671L,	1252015L ),
List( 18L,	39L,	68L,	97L,	118L,	142L,	233L,	557L,	1446L,	3477L,	7566L,	15077L,	27946L,	48820L,	81211L,	129665L,	199946L,	299235L,	436344L ),
List( 21L,	29L,	29L,	21L,	24L,	91L,	324L,	889L,	2031L,	4089L,	7511L,	12869L,	20874L,	32391L,	48454L,	70281L,	99289L,	137109L ),
List( 8L,	0L,  -8L,	3L,	67L,	233L,	565L,	1142L,	2058L,	3422L,	5358L,	8005L,	11517L,	16063L,	21827L,	29008L,	37820L ),
List( -8L,	-8L,	11L,	64L,	166L,	332L,	577L,	916L,	1364L,	1936L,	2647L,	3512L,	4546L,	5764L,	7181L,	8812L ),
List( 0L,	19L,	53L,	102L,	166L,	245L,	339L,	448L,	572L,	711L,	865L,	1034L,	1218L,	1417L,	1631L ),
List( 19L,	34L,	49L,	64L,	79L,	94L,	109L,	124L,	139L,	154L,	169L,	184L,	199L,	214L ),
List( 15L,	15L,	15L,	15L,	15L,	15L,	15L,	15L,	15L,	15L,	15L,	15L,	15L ),
List( 0L,	0L,	0L,	0L,	0L,	0L,	0L,	0L,	0L,	0L,	0L,	0L )
      ))


  }

  test("Day09 - parse") {
    assertEquals(parse(List("0 3 6 9 12 15")), List(List(0L, 3L, 6L, 9L, 12L, 15L)))
    assertEquals(parse(List("1 3 6 10 15 21")), List(List(1L, 3L, 6L, 10L, 15L, 21L)))
    assertEquals(parse(List("10 13 16 21 30 45")), List(List(10L, 13L, 16L, 21L, 30L, 45L)))
  }

  test("Day09 - prevValue") {
    assertEquals(prevValue(List(0L, 3L, 6L, 9L, 12L, 15L)), -3L)
    assertEquals(prevValue(List(1L, 3L, 6L, 10L, 15L, 21L)), 0L)
    assertEquals(prevValue(List(10L, 13L, 16L, 21L, 30L, 45L)), 5L)
    assertEquals(prevValue(
      List(-3L,7L,35L,102L,237L,469L,819L,1311L,2036L,3318L,6046L,12251L,26022L,54870L,111664L,217278L,404103L,720593L,1237029L,2052700L,3304715L)),
      -4L
    )

  }

  test("Day09 - Part1") {
    assertEquals(step1(test), 114L)
  }
  
  test("Day09 - Part2") {
    assertEquals(step2(test), 2L)
  }
}
