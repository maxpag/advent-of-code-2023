package org.maxpagani

import Card.strength

import scala.annotation.tailrec

object Card {

  val basicStrength = Map[Char, Int](
    '2' -> 2,
    '3' -> 3,
    '4' -> 4,
    '5' -> 5,
    '6' -> 6,
    '7' -> 7,
    '8' -> 8,
    '9' -> 9,
    'T' -> 10,
    'J' -> 11,
    'Q' -> 12,
    'K' -> 13,
    'A' -> 14
  )

  val jollyStrength  = Map[Char, Int](
    'J' -> 2,
    '2' -> 3,
    '3' -> 4,
    '4' -> 5,
    '5' -> 6,
    '6' -> 7,
    '7' -> 8,
    '8' -> 9,
    '9' -> 10,
    'T' -> 11,
    'Q' -> 12,
    'K' -> 13,
    'A' -> 14
  )
  var strength: Char => Int = basicStrength
}

enum HandType {
  case HighCard
  case OnePair
  case TwoPairs
  case ThreeOfAKind
  case FullHouse
  case FourOfAKind
  case FiveOfAKind
}

case class Card(value: Char) {
  def compare(other: Card): Int =
    strength(value) - strength(other.value)

  def isStrongerThen(other: Card): Boolean =
    compare(other) > 0
}

def parseHand(text: String,typeOfHand: List[Card] => HandType): Hand =
  val cards = text.toList.map(Card(_))
  Hand(cards, typeOfHand(cards))

def countCards( cards: List[Card] ) : Map[Char, Int] =
  cards.groupBy( _.value ).map( (k,v) => (k, v.size) )

@tailrec
def compareCardLists(a: List[Card], b: List[Card] ) : Int =
  (a,b) match {
    case (Nil, Nil) => 0
    case (Nil, _) => -1
    case (_, Nil) => 1
    case (headA::tailA, headB::tailB) =>
      val compareResult = headA.compare(headB)
      if compareResult != 0 then compareResult
      else compareCardLists( tailA, tailB )
  }

case class Hand( cards: List[Card], typeOfHand: HandType ) {
  private def compareSameType(hand: Hand): Int =
    compareCardLists( cards, hand.cards )


  def compare( other: Hand ) : Int =
    val thisType = typeOfHand
    val otherType = other.typeOfHand
    if thisType == otherType then
      compareSameType( other )
    else
      thisType.ordinal - otherType.ordinal

  def rank : Int =
    typeOfHand.ordinal
}

def typeOfHand( cards: List[Card]) : HandType =
  val cardsCount = countCards(cards)
  cardsCount.size match {
    case 5 => HandType.HighCard
    case 4 => HandType.OnePair
    case 3 => // 2 or 3
      cardsCount.values.max match {
        case 2 => HandType.TwoPairs
        case 3 => HandType.ThreeOfAKind
      }
    case 2 => //4 or 3+2
      cardsCount.values.max match {
        case 3 => HandType.FullHouse
        case 4 => HandType.FourOfAKind
      }
    case 1 => HandType.FiveOfAKind
  }


def jollyUp( hand: List[Card] ) : HandType =
  val cardsCount = countCards(hand)
  if( !cardsCount.contains('J') ) then
    typeOfHand(hand)
  else
    val jollyCount = cardsCount('J')
    val otherCards = hand.filter( _.value != 'J' )
    val otherCardsCount = countCards(otherCards)
    otherCardsCount.size match {
      case 4 => HandType.OnePair
      case 3 =>
        // 3 cards -> 2J + 1 other => Three of a kind
        // 4 cards -> 1J + 2 other => Three of a kind
        HandType.ThreeOfAKind
      case 2 =>
        // 2 cards -> 3J + 1 other => Four of a kind
        // 3 cards -> 2J + 2 other => Four of a kind
        // 4 cards -> 1J
        //    + 1/3 => Four of a kind
        //    + 2/2 => Full house
        if otherCards.size == 4 && otherCardsCount.values.max == 2 then
          HandType.FullHouse
        else
          HandType.FourOfAKind
      case 1 =>
        // 1 card -> 4J + 1 other => Five of a kind
        HandType.FiveOfAKind
      case 0 =>
        HandType.FiveOfAKind
    }

def parseData( data: List[String], typeOfHand: List[Card] => HandType ) : List[(Hand,Int)] =
  data.map( line =>
    val parts = line.split(" ")
    (parseHand(parts(0),typeOfHand),parts(1).toInt)
  )

def step1( data: List[String] ) : Long =
  Card.strength = Card.basicStrength
  val parsedData = parseData(data, typeOfHand)
  parsedData.sortWith( (a,b) => a._1.compare(b._1) < 0 ).zipWithIndex.map( (a,i) => a._2 * (i+1) ).sum

def step2( data: List[String] ) : Long =
  Card.strength = Card.jollyStrength
  val parsedData = parseData(data, jollyUp)
  parsedData.sortWith((a, b) => a._1.compare(b._1) < 0).zipWithIndex.map((a, i) => a._2 * (i + 1)).sum

