package org.maxpagani

import munit._

import org.maxpagani.step1
import org.maxpagani.step2
import munit.Clue.generate

class Day07Tests extends munit.FunSuite {
  private val test = List(
    "32T3K 765",
    "T55J5 684",
    "KK677 28",
    "KTJJT 220",
    "QQQJA 483"
  )

  test("count cards") {
    val expected = List(
      Map( '3' -> 2, '2' -> 1, 'T' -> 1, 'K' -> 1),
      Map( '5' -> 3, 'J' -> 1, 'T' -> 1),
      Map( '7' -> 2, '6' -> 1, 'K' -> 2),
      Map( 'J' -> 2, 'T' -> 2, 'K' -> 1),
      Map( 'Q' -> 3, 'J' -> 1, 'A' -> 1)
    )
    val hands = test.map( line => parseHand( line.split(" ").head,typeOfHand))
    val counts = hands.map( hand => countCards(hand.cards))
    expected zip counts foreach { case (e, c) => assertEquals(e, c)}
  }

  test("count cards jolly") {
    val expected = List(
      HandType.OnePair,
      HandType.FourOfAKind,
      HandType.TwoPairs,
      HandType.FourOfAKind,
      HandType.FourOfAKind
    )
    val hands = test.map( line =>
      val foo=parseHand( line.split(" ").head,jollyUp)
      println( s"$line => $foo")
      foo
    )
    expected zip hands foreach { case (e, c) => assertEquals(c.typeOfHand,e)}
  }

  test("extra count jolly") {
    val test = List(
      ("32T3K 765", HandType.OnePair),
      ("J1111 684", HandType.FiveOfAKind),
      ("J1222 28", HandType.FourOfAKind),
      ("J1233 220", HandType.ThreeOfAKind),
      ("J1234 483", HandType.OnePair),
      ("J1122 123", HandType.FullHouse),
      ("JJ111 483", HandType.FiveOfAKind),
      ("JJ112 213", HandType.FourOfAKind),
      ("JJ123 123", HandType.ThreeOfAKind),
      ("JJJ11 123", HandType.FiveOfAKind),
      ("JJJ12 123", HandType.FourOfAKind),
      ("JJJJ1 123", HandType.FiveOfAKind),
      ("JJJJJ 123", HandType.FiveOfAKind)
    )
    test.map( p =>
      (parseHand( p(0).split(" ").head,jollyUp),p(1))
    )
    .foreach { case (c,e) => assertEquals(c.typeOfHand,e)}
  }


  test("Day07 - Part1") {
    assertEquals(step1(test), 6440L)
  }

  test("Day07 - Part2") {
    assertEquals(step2(test), 5905L)
  }
}
